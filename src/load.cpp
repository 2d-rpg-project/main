#include <string>
#include <iostream>
#include <sstream>
#include <tinyxml2.h>

#include <CMap.h>
#include <CMapTile.h>
#include <Manager/CTextureManager.h>
#include <CChunk.h>
//http://cel6.net/2007/0328-ZLibString.html
#include <compress_decompress_string.h>
#include <fstream>
#include <CMapLoader.h>
using namespace std;

void CMap::loadAsync(std::string filename, sf::RenderWindow *window)
{
    if (filename != "")
    {
        if(this->mapLoader != nullptr)
        {
            if(this->mapLoader->isFinished())
                delete this->mapLoader;
            else
                return;
        }
        this->mapLoader = new CMapLoader(this, window);
        boost::thread mapLoadingThread(boost::bind(&CMapLoader::loadFromFile, this->mapLoader, filename));
        mapLoadingThread.detach();
    }
}
void CMap::loadStringAsync(string mapString, sf::RenderWindow *window)
{
    if(mapString != "")
    {
        if(this->mapLoader != nullptr)
        {
            if(this->mapLoader->isFinished())
                delete this->mapLoader;
            else
                return;
        }
        this->mapLoader = new CMapLoader(this, window);
        boost::thread mapLoadingThread(boost::bind(&CMapLoader::loadFromString, this->mapLoader, mapString));
        mapLoadingThread.detach();
    }
}
