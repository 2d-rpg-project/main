#include "CTileSelection.h"

CTileSelection::CTileSelection(sf::Vector2i size, sf::Vector2f pos)
{

}

CTileSelection::~CTileSelection()
{
    for(unsigned int i = 0; i < this->tiles.size(); i++)
    {
        if(this->tiles[i].tile != NULL)
            delete this->tiles[i].tile;
    }
    this->tiles.clear();
}
void CTileSelection::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    for(unsigned int i = 0; i < this->tiles.size(); i++)
    {
        target.draw(this->tiles[i].sprite);
        target.draw(this->tiles[i].box);
    }
}
void CTileSelection::setPosition(sf::Vector2f pos)
{
    this->pos = pos;
}
void CTileSelection::addTile(CMapTile *tile, sf::Vector2i gridPos)
{
    selectionTile selTile;
    selTile.tile = tile;
    selTile.box.setSize(sf::Vector2f(32, 32));
    selTile.box.setFillColor(sf::Color(0, 0, 200, 40));
    selTile.box.setOutlineColor(sf::Color::Blue);
    selTile.box.setOutlineThickness(1);
    selTile.box.setPosition(sf::Vector2f(this->pos.x + gridPos.x * 32, this->pos.y + gridPos.y * 32));
    selTile.gridPos = gridPos;
    selTile.sprite.setPosition(sf::Vector2f(this->pos.x + gridPos.x * 32, this->pos.y + gridPos.y * 32));
    if(tile->layer == 0)
        selTile.sprite.setTexture(*layer0);
    if(tile->layer == 1)
        selTile.sprite.setTexture(*layer1);
    if(tile->layer == 2)
        selTile.sprite.setTexture(*layer2);
    selTile.sprite.setTextureRect(sf::IntRect(tile->tex.x, tile->tex.y, 32, 32));

    this->tiles.push_back(selTile);
}
void CTileSelection::setTextures(sf::Texture *layer0, sf::Texture *layer1, sf::Texture *layer2)
{
    this->layer0 = layer0;
    this->layer1 = layer1;
    this->layer2 = layer2;
}
