#include <CMapLoader.h>
#include <vector>
#include <string>
#include <Manager/CTextureManager.h>

using namespace std;

void CMapLoader::load_V02(tinyxml2::XMLDocument &doc)
{
    unsigned int mapWidth = 0, mapHeight = 0;
    const char* title = doc.FirstChildElement("title")->GetText();
    const char* author = doc.FirstChildElement("author")->GetText();
    this->setCurrentStatus("parsing tilesets");
    vector<string> tilesets;
    vector<bool> foreground;
    vector<int> tilesetX;
    tinyxml2::XMLElement *tileset = doc.FirstChildElement("layer")->FirstChildElement("tileset");
    do
    {
        tilesets.push_back(tileset->Attribute("p"));
        if(atoi(tileset->Attribute("f")) == 0)
            foreground.push_back(false);
        else
            foreground.push_back(true);
        tilesetX.push_back(CTextureManager::Get()->getTexture(tileset->Attribute("p")).getSize().x / 32);
        tileset = tileset->NextSiblingElement("tileset");
    }
    while(tileset != NULL);
    mapWidth = atoi(doc.FirstChildElement("mapWidth")->GetText());
    mapHeight = atoi(doc.FirstChildElement("mapHeight")->GetText());
    cout << "[Loader - V0.2]: Loading Map asynchronous..." << endl;
    cout << "   Title:  " << title << endl;
    cout << "   Author: " << author << endl;
    this->setCurrentStatus("loading tilesets");
    for(unsigned int i = 0; i < tilesets.size(); i++)
    {
        cout << "   Tileset " << i << ": " << tilesets[i] << " - Foreground: " << foreground[i] << endl;
        if(CTextureManager::Get()->createTexture(tilesets[i], tilesets[i], this->window) <= 0)
        {
            cout << "    !! Couldn't load the " << i << " tileset: " << tilesets[i] << endl;
        }
        this->gameMap->setTileset(i, tilesets[i], foreground[i]);
    }
    tinyxml2::XMLElement *tile = doc.FirstChildElement("map")->FirstChildElement("tile");
    this->gameMap->setAuthor(author);
    this->gameMap->setTitle(title);
    this->setCurrentStatus("resizing & initializing the map array");
    this->gameMap->mapMutex.lock();
    this->gameMap->chunks.resize(mapWidth / 16 + 1);
    for(unsigned int i = 0; i < this->gameMap->chunks.size(); i++)
    {
        this->gameMap->chunks[i].resize(mapHeight / 16 + 1);
    }
    this->gameMap->mapMutex.unlock();
    this->gameMap->sizeX = mapWidth;
    this->gameMap->sizeY = mapHeight;
    this->gameMap->reAlignChunks();
    unsigned int posX = 0, posY = 0;
    unsigned int texPosX = 0, texPosY = 0;
    unsigned int textureID;
    unsigned int layer = 0;
    unsigned int rotation = 0;
    stringstream currentTileStream;
    do
    {
        posX = atoi(tile->Attribute("X"));
        posY = atoi(tile->Attribute("Y"));
        layer = atoi(tile->Attribute("l"));
        rotation = atoi(tile->Attribute("r"));
        textureID = atoi(tile->Attribute("t")) + 1;
        currentTileStream.str("Creating tile ");
        currentTileStream << tile->Attribute("X") << "x" << tile->Attribute("Y") << tile->Attribute("l");
        this->setCurrentStatus(currentTileStream.str());
        if(posX > mapWidth || posY > mapHeight)
        {
            cout << "Invalid map size and/or invalid tile at " << posX << "x" << posY << "x" << layer << "!" << endl;
            continue;
        }
        texPosY = 32 * ((textureID - 1) / tilesetX[layer]);
        if(textureID % tilesetX[layer] != 0)
            texPosX = 32 * (textureID%tilesetX[layer] - 1);
        else
            texPosX = 32 * (tilesetX[layer] - 1);
        CMapTile *mapTile = new CMapTile(sf::Vector2i(posX, posY), sf::Vector2i(texPosX, texPosY), layer, rotation);
        this->gameMap->setTile(mapTile, false); //Set the texture layers
        tile = tile->NextSiblingElement("tile");
    }
    while(tile != NULL);
    cout << "[Map-Loader] Finished Loading \"" << title << "\"!" << endl;
    this->setCurrentStatus("finished loading");
    this->gameMap->reRenderAll();
    this->onFinished(this);
    this->finished = true;
}
