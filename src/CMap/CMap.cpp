#include "CMap.h"
#include <Manager/CTextureManager.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <tinyxml2.h>
#include <thread>
#include <mutex>
#include <boost/algorithm/string.hpp>
#include <CConfig.h>
#include <CMapLoader.h>

using namespace std;

CMap::CMap(sf::RenderWindow *window)
{
    this->MainView = NULL;
    this->unsetSpaceShape = NULL;
    this->mapLoader = NULL;
    this->window = window;
    this->clearChunks();
    window->setActive(true);
    this->unsetSpaceShape = new sf::RectangleShape(sf::Vector2f(10, 10));
    this->unsetSpaceShape->setFillColor(sf::Color(0, 0, 255, 40));
    this->unsetSpaceShape->setPosition(0, 0);
    window->setActive(false);
    this->drawUnsetTiles = false;
    for(unsigned int i = 0; i < this->mapLayers.size(); i++)
    {
        this->mapLayers[i].visible = true;
    }
}

CMap::~CMap()
{
    this->clearChunks();
    if(this->mapLoader != NULL)
        delete this->mapLoader;
    if(this->unsetSpaceShape != NULL)
        delete this->unsetSpaceShape;
}
void CMap::draw(sf::RenderTarget *target, bool foreground = false)
{
    if(!foreground)
    {
        this->mapMutex.lock();
        unsigned int x, y;
        if(this->drawUnsetTiles)
        {
            target->draw(*this->unsetSpaceShape);
        }
        for(x = 0; x < this->chunks.size(); x++)
        {
            for(y = 0; y < this->chunks[x].size(); y++)
            {
                if(chunks[x][y].visible)
                {
                    target->draw(this->chunks[x][y]);
                }
            }
        }
        this->mapMutex.unlock();
    }
    else
    {
        this->mapMutex.lock();
        unsigned int x, y;
        for(x = 0; x < this->chunks.size(); x++)
        {
            for(y = 0; y < this->chunks[x].size(); y++)
            {
                if(chunks[x][y].visible)
                {
                    this->chunks[x][y].drawForeground(*target, sf::RenderStates::Default);
                }
            }
        }
        this->mapMutex.unlock();
    }
}
sf::Vector2i CMap::convertToChunk(sf::Vector2i tilePos)
{
    return tilePos / 16;
}
void CMap::clearChunks()
{
    this->mapMutex.lock();
    this->getSetMutex.lock();
    unsigned int x, y;
    for(x = 0; x < this->chunks.size(); x++)
    {
        for(y = 0; y < this->chunks[x].size(); y++)
        {
            this->chunks[x][y].clear();
            cout << "Cleared Chunk " << x << "x" << y << "..." << endl;
        }
    }
    this->chunks.clear();
    this->sizeX = 0;
    this->sizeY = 0;
    this->visibleChunks = 0;
    this->getSetMutex.unlock();
    this->mapMutex.unlock();
}
void CMap::update(sf::FloatRect &MainViewRect)
{
    unsigned int x, y;
    if(this->drawUnsetTiles)
    {
        this->unsetSpaceShape->setSize(sf::Vector2f(this->sizeX * 32, this->sizeY * 32));
    }
    if(this->mapLoader != NULL)
    {
        if(this->mapLoader->isFinished())
        {
            delete this->mapLoader;
            this->mapLoader = NULL;
        }
    }
    this->mapMutex.lock();
    this->visibleChunks = 0;
    for(x = 0; x < this->chunks.size(); x++)
    {
        for(y = 0; y < this->chunks[x].size(); y++)
        {
            if(this->chunks[x][y].getRectangle().intersects(MainViewRect))
            {
                chunks[x][y].visible = true;
                this->visibleChunks++;
            }
            else
            {
                chunks[x][y].visible = false;
            }
        }
    }
    this->mapMutex.unlock();
}
void CMap::setTile(CMapTile *tile, bool reRender = false)
{
    this->mapMutex.lock();
    if((unsigned int) tile->pos.x / 16 < this->chunks.size())
    {
        if((unsigned int) tile->pos.y / 16 < this->chunks[tile->pos.x / 16].size())
        {
            if(tile->layer < 3)
            {
                if((unsigned int) tile->pos.x < this->sizeX && (unsigned int) tile->pos.y < this->sizeY)
                    this->chunks[tile->pos.x / 16][tile->pos.y / 16].setTile(tile, reRender);
            }
        }
    }
    this->mapMutex.unlock();
}
CMapTile* CMap::getTile(sf::Vector2i pos, unsigned int layer)
{
    if((unsigned int) pos.x / 16 < this->chunks.size())
    {
        if((unsigned int) pos.y / 16 < this->chunks[pos.x / 16].size())
        {
            if(layer < 3)
            {
                return this->chunks[pos.x / 16][pos.y / 16].getTile(pos, layer);
            }
        }
    }
    return NULL;
}

void CMap::reRenderAllVisible()
{
    this->mapMutex.lock();
    unsigned int x, y;
    for(x = 0; x < this->chunks.size(); x++)
    {
        for(y = 0; y < this->chunks[x].size(); y++)
        {
            if(this->chunks[x][y].visible)
            {
                this->chunks[x][y].reRender();
            }
        }
    }
    this->mapMutex.unlock();
}
void CMap::reRenderAll()
{
    this->mapMutex.lock();
    unsigned int x, y;
    if(this->chunks.size() > 0)
    {
        for(x = 0; x < this->chunks.size(); x++)
        {
            for(y = 0; y < this->chunks[x].size(); y++)
            {
                this->chunks[x][y].reRender();
            }
        }
    }
    this->mapMutex.unlock();
}
void CMap::reRender(sf::Vector2i chunk)
{
    this->mapMutex.lock();
    if((unsigned int) chunk.x < this->chunks.size())
    {
        if((unsigned int) chunk.y < this->chunks[chunk.x].size())
        {
            this->chunks[chunk.x][chunk.y].reRender();
        }
    }
    this->mapMutex.unlock();
}
void CMap::setSize(sf::Vector2i newSize)
{
    if((unsigned int) newSize.x / 16 == this->sizeX / 16
        && (unsigned int) newSize.y / 16 == this->sizeY / 16)      //Check if the new size makes any difference with the chunks
    {
        this->getSetMutex.lock();
        this->sizeX = newSize.x;
        this->sizeY = newSize.y;
        this->getSetMutex.unlock();
    }
    else
    {
        this->getSetMutex.lock();
        this->sizeX = newSize.x;
        this->sizeY = newSize.y;
        this->getSetMutex.unlock();
        this->loadStringAsync(this->saveToString(), this->window);
    }
}
void CMap::reAlignChunks()
{
    this->mapMutex.lock();
    unsigned int x, y, z;
    for(x = 0; x < this->chunks.size(); x++)
    {
        for(y = 0; y < this->chunks[x].size(); y++)
        {
            this->chunks[x][y].setPos(sf::Vector2i(x * 16 * 32, y * 16 * 32)); //Set the chunk position in pixels
            for(z = 0; z < this->mapLayers.size(); z++)
            {
                this->chunks[x][y].setLayer(z, this->mapLayers[z].foreground, this->mapLayers[z].visible, &CTextureManager::Get()->getTexture(this->mapLayers[z].textureName));
                this->chunks[x][y].setTexture(z, &CTextureManager::Get()->getTexture(this->getTileset(z)));
            }
        }
    }
    this->mapMutex.unlock();
}
void CMap::setTileset(unsigned int layer, std::string textureName, bool foreground)
{
    if(layer == this->mapLayers.size() || this->mapLayers.size() == 0)
    {
        SMapLayer layer;
        layer.foreground = foreground;
        layer.textureName = textureName;
        layer.visible = true;
        this->mapLayers.push_back(layer);
        this->reAlignChunks();
        return;
    }
    if(layer < this->mapLayers.size())
    {
        if(this->getTileset(layer) != "")
            CTextureManager::Get()->deleteTexture(this->getTileset(layer));
        CTextureManager::Get()->createTexture(textureName, textureName, this->window);
        this->getSetMutex.lock();
        this->mapLayers[layer].textureName = textureName;
        this->mapLayers[layer].foreground = foreground;
        this->getSetMutex.unlock();
        this->reAlignChunks();
    }
    else
    {
        cout << "!! Couldn't set tileset \"" << textureName << "\" in the Map on layer " << layer << endl;
    }
}
std::string CMap::getCurrentState()
{
    if(this->mapLoader != NULL)
    {
        return this->mapLoader->getCurrentStatus();
    }
    else
    {
        return "Simulating the map";
    }
}

std::string CMap::getAuthor()
{
    this->getSetMutex.lock();
    std::string author = this->mapAuthor;
    this->getSetMutex.unlock();
    return author;
}
std::string CMap::getTitle()
{
    this->getSetMutex.lock();
    std::string title = this->mapTitle;
    this->getSetMutex.unlock();
    return title;
}
std::string CMap::getTileset(unsigned int layer)
{
    if(layer < this->mapLayers.size())
    {
        std::string tileset = this->mapLayers[layer].textureName;
        return tileset;
    }
    else
    {
        return "TextureError";
    }
}
void CMap::setAuthor(string author)
{
    this->getSetMutex.lock();
    this->mapAuthor = author;
    this->getSetMutex.unlock();
}
void CMap::setTitle(string title)
{
    this->getSetMutex.lock();
    this->mapTitle = title;
    this->getSetMutex.unlock();
}
sf::Vector2i CMap::getSize()
{
    this->getSetMutex.lock();
    sf::Vector2i size;
    size.x = this->sizeX;
    size.y = this->sizeY;
    this->getSetMutex.unlock();
    return size;
}
boost::signal<void (CMapLoader *)> *CMap::getOnLoadedEvent()
{
    if(this->mapLoader != NULL)
    {
        return this->mapLoader->getOnLoadedEvent();
    }
    else
        return NULL;
}
void CMap::deleteTile(sf::Vector2i tile, int layer)
{
    if((unsigned int)tile.x / 16 < this->chunks.size())
    {
        if((unsigned int) tile.y / 16 < this->chunks[tile.x / 16].size())
        {
            this->chunks[tile.x / 16][tile.y / 16].deleteTile(tile, layer);
        }
    }
}
void CMap::setVisibleLayer(int layer, bool visible)
{
    this->getSetMutex.lock();
    try {
        this->mapLayers.at(layer).visible = visible;
    }
    catch(std::out_of_range &e) {
        cout << "! Cannot set layer " << layer << " visibility to " << visible << "! (Out of Range)" << endl;
        return;
    }
    this->getSetMutex.unlock();
    unsigned int x, y;
    this->mapMutex.lock();
    for(x = 0; x < this->chunks.size(); x++)
    {
        for(y = 0; y < this->chunks[x].size(); y++)
        {
            this->chunks[x][y].setVisibleLayer(layer, visible);
        }
    }
    this->mapMutex.unlock();
}
bool CMap::getVisibleLayer(unsigned int layer)
{
    bool visible = false;
    this->getSetMutex.lock();
    if(layer < this->mapLayers.size())
        visible = this->mapLayers[layer].visible;
    this->getSetMutex.unlock();
    return visible;
}
bool CMap::getDrawUnsetTiles()
{
    this->getSetMutex.lock();
    bool drawUnset = this->drawUnsetTiles;
    this->getSetMutex.unlock();
    return drawUnset;
}
unsigned int CMap::getLayerCount()
{
    this->getSetMutex.lock();
    unsigned int layerCount = this->mapLayers.size();
    this->getSetMutex.unlock();
    return layerCount;
}
bool CMap::getForeground(unsigned int layer)
{
    bool f = false;
    if(layer < this->mapLayers.size())
        f = this->mapLayers[layer].foreground;
    return f;
}
