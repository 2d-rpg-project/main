#include "CMapLoader.h"

#include <iostream>
#include <fstream>
#include <Manager/CTextureManager.h>
#include <sstream>
using namespace std;

CMapLoader::CMapLoader(CMap *gameMap, sf::RenderWindow *window)
{
    if(gameMap != NULL)
    {
        this->gameMap = gameMap;
    }
    this->window = window;
    this->finished = false;
}

CMapLoader::~CMapLoader()
{

}

bool CMapLoader::loadFromFile(std::string filename)
{
    if(filename != "")
    {
        std::ifstream ifs(filename.c_str());
        string mapString = "";
        if(filename.substr(filename.find_last_of('.')+1, filename.length()) == "map")
        {
            cout << "Decompressing map... ";
            this->setCurrentStatus("decompressing the map");
            std::string compressedMap( (std::istreambuf_iterator<char>(ifs) ),
                           (std::istreambuf_iterator<char>()    ) );
            //mapString = decompress_string(compressedMap);
            return this->loadFromString(mapString);
        }
        else if(filename.substr(filename.find_last_of('.')+1, filename.length()) == "xml")
        {
            this->setCurrentStatus("loading map file directly");
            int length;
            char * fileBuffer;
            cout << "Loading map directly... ";
            ifs.seekg(0, ios::end);
            length = ifs.tellg();
            ifs.seekg(0, ios::beg);
            fileBuffer = new char[length];
            ifs.read(fileBuffer, length);
            mapString = fileBuffer;
            cout << " ...finished!" << endl;
            delete[] fileBuffer;
            try
            {
                this->loadFromString(mapString);
            }
            catch(CMapLoaderException& e)
            {
                cout << "Loading Exception thrown! - " << e.what() << endl;
            }
        }
        else
        {
            cout << "!! Unknown map format \"" << filename.substr(filename.find_last_of('.')+1, filename.length()) << "\"!" << endl;
            return false;
        }
    }
    else
        return false;
}
bool CMapLoader::loadFromString(std::string mapString)
{
    if(this->gameMap != NULL)
    {
        this->setCurrentStatus("clearing chunks");
        this->gameMap->clearChunks();
        tinyxml2::XMLDocument doc;
        doc.Parse(mapString.c_str());
        this->setCurrentStatus("parsing the map");
        string formatVersion = doc.FirstChildElement("format_version")->GetText();
        cout << "[CMapLoader] Format-Version: " << formatVersion << endl;
        if(formatVersion == "0.2")
            this->load_V02(doc);
    }
    else
        return false;
}
std::string CMapLoader::getTextureName(int layer)
{
    return this->gameMap->getTileset(layer);
}

void CMapLoader::setCurrentStatus(std::string status)
{
    this->getMutex.lock();
    this->currentStatus = status;
    this->getMutex.unlock();
}
std::string CMapLoader::getCurrentStatus()
{
    this->getMutex.lock();
    std::string status = this->currentStatus;
    this->getMutex.unlock();
    return status;
}
unsigned long int CMapLoader::getCurrentTile()
{
    this->getMutex.lock();
    unsigned long int currentTile = this->currentTile;
    this->getMutex.unlock();
    return currentTile;
}
unsigned long int CMapLoader::getTileCount()
{
    this->getMutex.lock();
    unsigned long int tileCount = this->tileCount;
    this->getMutex.unlock();
    return tileCount;
}
bool CMapLoader::isFinished()
{
    this->getMutex.lock();
    bool finished = this->finished;
    this->getMutex.unlock();
    return finished;
}
boost::signal<void (CMapLoader *)> *CMapLoader::getOnLoadedEvent()
{
    return &this->onFinished;
}
unsigned int CMapLoader::getLayerCount()
{
    return this->gameMap->getLayerCount();
}
