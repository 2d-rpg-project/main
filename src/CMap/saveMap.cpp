#include <CMap.h>
#include <Manager/CTextureManager.h>
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <tinyxml2.h>

#include <boost/algorithm/string.hpp>

using namespace std;

std::string CMap::saveToString()
{
    unsigned int x, y;
    tinyxml2::XMLPrinter printer;
    //Title
        printer.OpenElement("title");
        printer.PushText(this->mapTitle.c_str());
        printer.CloseElement();
    //Author
        printer.OpenElement("author");
        printer.PushText(this->mapAuthor.c_str());
        printer.CloseElement();
    //Map Width
        printer.OpenElement("mapWidth");
        printer.PushText(this->sizeX);
        printer.CloseElement();
    //Map Height
        printer.OpenElement("mapHeight");
        printer.PushText(this->sizeY);
        printer.CloseElement();
    //Tilesets
        printer.OpenElement("layer");
        for(unsigned int i = 0; i < this->mapLayers.size(); i++)
        {
            printer.OpenElement("tileset");
            printer.PushAttribute("f", this->mapLayers[i].foreground);
            printer.PushAttribute("p", this->mapLayers[i].textureName.c_str());
            printer.CloseElement();
        }
        printer.CloseElement();
    stringstream filestream;
    this->mapMutex.lock();
    filestream << printer.CStr();
    //Tiles
        filestream << "<map>" << endl;
        for(x = 0; x < this->chunks.size(); x++)
        {
            for(y = 0; y < this->chunks.size(); y++)
            {
                filestream << this->chunks[x][y].getFullXML();
            }
        }
        filestream << "</map>" << endl;
    string tempFile = filestream.str();
    boost::algorithm::replace_all(tempFile, "\n\n", "\n");
    cout << tempFile << endl;
    this->mapMutex.unlock();
    return tempFile;
}
void CMap::saveToFile(string filename, bool compress = false)
{
    cout << "Saving the map... " << endl;
    ofstream file;
    file.open (filename.c_str());
    file << this->saveToString();
    file.close();
    cout << "Finished saving the map! Filename: " << filename << endl;
}
