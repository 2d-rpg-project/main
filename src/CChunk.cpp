#include "CChunk.h"
#include <iostream>
#include <CMapTile.h>
#include <zlib/zlib.h>
#include <tinyxml2.h>
#include <sstream>
#include <vector>

using namespace std;
CChunk::CChunk()
{

}
void CChunk::setPos(sf::Vector2i pos)
{
    this->pos = pos;
}
void CChunk::directReset()
{
    unsigned int x, y, z;
    for(z = 0; z < this->layer.size(); z++)
    {
        for(x = 0; x < 16; x++)
        {
            for(y = 0; y < 16; y++)
            {
                this->layer[z].tiles[x][y] = NULL;
            }
        }
        this->layer[z].vertices.setPrimitiveType(sf::Quads);
        this->layer[z].visible = true;
        this->layer[z].texture = NULL;
    }
}
void CChunk::setLayerCount(unsigned int count)
{
    this->layer.resize(count);
    this->directReset();
}

CChunk::~CChunk()
{
    this->clear();
}
void CChunk::setTexture(unsigned int layer, sf::Texture *texture)
{
    if(layer < this->layer.size())
        this->layer[layer].texture = texture;
    else
        cout << "! The Chunk at " << this->pos.x << "x" << this->pos.y << " has no " << layer << " layer! Could not set texture!" << endl;
}
void CChunk::setLayer(unsigned int layer, bool foreground, bool visible, sf::Texture *texture)
{
    if(layer = this->layer.size() + 1)
    {
        SChunkLayer chunklayer;
        chunklayer.foreground = foreground;
        chunklayer.texture = texture;
        chunklayer.visible = visible;
        unsigned int x, y;
        for(x = 0; x < 16; x++)
        {
            for(y = 0; y < 16; y++)
            {
                chunklayer.tiles[x][y] = NULL;
            }
        }
        this->layer.push_back(chunklayer);
    }


}
void CChunk::setVertex(CMapTile *tile)
{
    if(tile != NULL)
    {
        if(tile->layer < this->layer.size())
        {
            sf::Vertex vert[4];
            vert[0].position = sf::Vector2f(tile->pos.x * 32, tile->pos.y * 32);
            vert[1].position = sf::Vector2f(tile->pos.x * 32 + 32, tile->pos.y * 32);
            vert[2].position = sf::Vector2f(tile->pos.x * 32 + 32, tile->pos.y * 32 + 32);
            vert[3].position = sf::Vector2f(tile->pos.x * 32, tile->pos.y * 32 + 32);
            if(tile->rotation == 0)
            {
                vert[0].texCoords = sf::Vector2f(tile->tex.x, tile->tex.y);
                vert[1].texCoords = sf::Vector2f(tile->tex.x + 32, tile->tex.y);
                vert[2].texCoords = sf::Vector2f(tile->tex.x + 32, tile->tex.y + 32);
                vert[3].texCoords = sf::Vector2f(tile->tex.x, tile->tex.y + 32);
            }
            if(tile->rotation == 1)
            {
                vert[0].texCoords = sf::Vector2f(tile->tex.x + 32, tile->tex.y);
                vert[1].texCoords = sf::Vector2f(tile->tex.x + 32, tile->tex.y + 32);
                vert[2].texCoords = sf::Vector2f(tile->tex.x, tile->tex.y + 32);
                vert[3].texCoords = sf::Vector2f(tile->tex.x, tile->tex.y);
            }
            if(tile->rotation == 2)
            {
                vert[0].texCoords = sf::Vector2f(tile->tex.x + 32, tile->tex.y + 32);
                vert[1].texCoords = sf::Vector2f(tile->tex.x, tile->tex.y + 32);
                vert[2].texCoords = sf::Vector2f(tile->tex.x, tile->tex.y);
                vert[3].texCoords = sf::Vector2f(tile->tex.x + 32, tile->tex.y);
            }
            if(tile->rotation == 3)
            {
                vert[0].texCoords = sf::Vector2f(tile->tex.x, tile->tex.y + 32);
                vert[1].texCoords = sf::Vector2f(tile->tex.x, tile->tex.y);
                vert[2].texCoords = sf::Vector2f(tile->tex.x + 32, tile->tex.y);
                vert[3].texCoords = sf::Vector2f(tile->tex.x + 32, tile->tex.y + 32);
            }
            if(tile->layer < this->layer.size())
            {
                this->layer[tile->layer].vertices.append(vert[0]);
                this->layer[tile->layer].vertices.append(vert[1]);
                this->layer[tile->layer].vertices.append(vert[2]);
                this->layer[tile->layer].vertices.append(vert[3]);
            }
        }
    }
}
void CChunk::setTile(CMapTile *tile, bool reRender)
{
    if(tile != NULL)
    {
        if(tile->layer < this->layer.size() && tile->pos.x % 16 >= 0 && tile->pos.x % 16 <= 15 && tile->pos.y % 16 >= 0 && tile->pos.y % 16 <= 15)
        {
            if(this->layer[tile->layer].tiles[tile->pos.x % 16][tile->pos.y % 16] != NULL)
            {
                delete this->layer[tile->layer].tiles[tile->pos.x % 16][tile->pos.y % 16];
                this->layer[tile->layer].tiles[tile->pos.x % 16][tile->pos.y % 16] = NULL;
            }
            this->layer[tile->layer].tiles[tile->pos.x % 16][tile->pos.y % 16] = tile;
            if(reRender)
                this->reRender();
        }
        else
        {
            cout << "! Tile at " << tile->pos.x << "x" << tile->pos.y << "x" << tile->layer << " incorrect!" << endl;
        }
    }
}
void CChunk::reRender()
{
    unsigned int x, y, z;
    for(z = 0; z < this->layer.size(); z++)
    {
        this->layer[z].vertices.clear();
        this->layer[z].vertices.setPrimitiveType(sf::Quads);
    }
    for(z = 0; z < this->layer.size(); z++)
    {
        for(x = 0; x < 16; x++)
        {
            for(y = 0; y < 16; y++)
            {
                if(this->layer[z].tiles[x][y] != NULL)
                    this->setVertex(this->layer[z].tiles[x][y]);
            }
        }
    }

}
void CChunk::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    for(unsigned int z = 0; z < this->layer.size(); z++)
    {
        if(this->layer[z].foreground == false)
        {
            states.texture = this->layer[z].texture;
            target.draw(this->layer[z].vertices, states);
        }
    }
}
void CChunk::drawForeground(sf::RenderTarget &target, sf::RenderStates states)
{
    for(unsigned int z = 0; z < this->layer.size(); z++)
    {
        if(this->layer[z].foreground == true)
        {
            states.texture = this->layer[z].texture;
            target.draw(this->layer[z].vertices, states);
        }
    }
}
sf::FloatRect CChunk::getRectangle()
{
    sf::FloatRect rect;
    rect.left = pos.x;
    rect.top = pos.y;
    rect.height = 512; //32 x 16
    rect.width = 512; //32 x 16
    return rect;
}
CMapTile* CChunk::getTile(sf::Vector2i pos, unsigned int layer)
{
    if(layer < this->layer.size())
        return this->layer[layer].tiles[pos.x % 16][pos.y % 16];
    else
        return NULL;
}
void CChunk::clear()
{
    unsigned int x, y, z;
    for(z = 0; z < this->layer.size(); z++)
    {
        for(x = 0; x < 16; x++)
        {
            for(y = 0; y < 16; y++)
            {
                if(this->layer[z].tiles[x][y] != NULL)
                {
                    delete this->layer[z].tiles[x][y];
                }
                this->layer[z].tiles[x][y] = NULL;
            }
        }
        this->layer[z].vertices.clear();
    }
}
std::string CChunk::getFullXML()
{
    tinyxml2::XMLPrinter printer;
    unsigned int x, y, z;
    stringstream commentstream;
    commentstream << " == CHUNK " << this->pos.x / 32 / 16 << "x" << this->pos.y / 32 / 16 << " == ";
    printer.PushComment(commentstream.str().c_str());
    for(z = 0; z < this->layer.size(); z++)
    {
        for(x = 0; x < 16; x++)
        {
            for(y = 0; y < 16; y++)
            {
                if(this->layer[z].tiles[x][y] != NULL)
                {
                    printer.OpenElement("tile");
                    printer.PushAttribute("X", this->layer[z].tiles[x][y]->pos.x);
                    printer.PushAttribute("Y", this->layer[z].tiles[x][y]->pos.y);
                    printer.PushAttribute("l", this->layer[z].tiles[x][y]->layer);
                    printer.PushAttribute("r", this->layer[z].tiles[x][y]->rotation);
                    printer.PushAttribute("t", (this->layer[z].tiles[x][y]->tex.y / 32)
                                          * (this->layer[z].texture->getSize().x / 32)
                                          + (this->layer[z].tiles[x][y]->tex.x / 32));
                    printer.CloseElement();
                }
            }
        }
    }

    return printer.CStr();
}
void CChunk::deleteTile(sf::Vector2i tile, unsigned int layer)
{
    if(layer < this->layer.size())
    {
        if(this->layer[layer].tiles[tile.x % 16][tile.y % 16] != NULL)
        {
            delete this->layer[layer].tiles[tile.x % 16][tile.y % 16];
        }
        this->layer[layer].tiles[tile.x % 16][tile.y % 16] = NULL;
        this->reRender();
    }
}
void CChunk::setVisibleLayer(unsigned int layer, bool visible)
{
    if(layer < this->layer.size())
        this->layer[layer].visible = visible;
    else
        cout << "! Cannot set layer " << layer << " visibility to " << visible << "! (Out of Range)" << endl;
}
