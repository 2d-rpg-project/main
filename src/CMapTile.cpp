#include "CMapTile.h"

CMapTile::CMapTile(sf::Vector2i pos, sf::Vector2i tex, unsigned int layer, unsigned int rotation)
{
    this->pos = pos;
    if(layer > 2)
        layer = 2;
    if(layer < 0)
        layer = 0;
    this->layer = layer;
    this->rotation = rotation;
    this->tex = tex;
}

CMapTile::~CMapTile()
{
    //dtor
}
