#include <CGame.h>
#include <SFML/Graphics.hpp>
#include <string>
#include <Manager/CFontManager.h>
#include <Manager/CTextureManager.h>
void CGame::onExit()
{
    if(this->DebugText != NULL)
        delete this->DebugText;
    if(this->MainView != NULL)
        delete this->MainView;
    if(this->DefaultView != NULL)
        delete this->DefaultView;
    delete this->gameMap;
    delete this->window;
    CTextureManager::Get()->destroy();
    CFontManager::Get()->destroy();
}
