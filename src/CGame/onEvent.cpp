#include <CGame.h>
#include <CConfig.h>
#include <iostream>
using namespace std;

void CGame::onEvent()
{
    sf::Event Event;
    CConfig::Get()->lockWindowMutex();
    if(!this->window->setActive(true))
        cout << "!! Window couldn't be made active!" << endl;
    while(this->window->pollEvent(Event))
    {
        if(Event.type == sf::Event::Closed)
        {
            CConfig::Get()->quitGame();
        }
        if(Event.type == sf::Event::KeyPressed)
        {
            if(Event.key.code == sf::Keyboard::Escape)
            {
                CConfig::Get()->quitGame();
            }
            if(Event.key.code == sf::Keyboard::R)
            {
                this->gameMap->loadAsync("Data/Maps/test1.xml", this->window);
            }
        }
        if(Event.type == sf::Event::Resized)
        {
            this->MainViewRect.width = Event.size.width;
            this->MainViewRect.height = Event.size.height;
            this->MainView->reset(this->MainViewRect);
            this->DefaultView->reset(sf::FloatRect(0, 0, Event.size.width, Event.size.height));
            //this->gameMap->reRenderAll();
        }
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::W))
    {
        this->MainViewRect.top -= 5;
        this->MainView->reset(this->MainViewRect);
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::A))
    {
        this->MainViewRect.left -= 5;
        this->MainView->reset(this->MainViewRect);
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::S))
    {
        this->MainViewRect.top += 5;
        this->MainView->reset(this->MainViewRect);
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::D))
    {
        this->MainViewRect.left += 5;
        this->MainView->reset(this->MainViewRect);
    }
    if(!this->window->setActive(false))
        cout << "!! Window couldn't be made inactive!" << endl;
    CConfig::Get()->unlockWindowMutex();
}
