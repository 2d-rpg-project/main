#include <CGame.h>
#include <SFML/Graphics.hpp>
#include <string>
#include <iostream>
#include <CConfig.h>
#include <CMap.h>
#include <Manager/CFontManager.h>
#include <Manager/CTextureManager.h>
using namespace std;

bool CGame::onInit()
{
    this->window = new sf::RenderWindow(sf::VideoMode(800, 500), "2D RPG Project - PREPRE ALPHA 0.00001");
    if(this->window == NULL)
        return false;
    this->window->setFramerateLimit(60);
        //Init the standard FontManager Fonts
        CFontManager::Get()->createFont("Data/Fonts/Beeb Mode One.ttf", "Beeb Mode One");
        CFontManager::Get()->createFont("Data/Fonts/Inconsolata.otf", "Inconsolata");
    //Give the renderwindow pointer to the texture-manager
        CTextureManager::Get()->setRenderWindow(this->window);

    //Set the Debug Mode
        CConfig::Get()->setDebug(DebugMode::master, true);

    //Init the DebugText
        this->DebugText = NULL;
        this->DebugText = new sf::Text();
        this->DebugText->setPosition(0, 0);
        this->DebugText->setColor(sf::Color::White);
        this->DebugText->setFont(CFontManager::Get()->getFont("Beeb Mode One"));
        this->DebugText->setCharacterSize(16);
    //Init Game-Map
        this->gameMap = new CMap(this->window);

    //Set Game-State to GameState::Game
        CConfig::Get()->setGameState(GameState::game);

    //Set the Main View up
        this->MainView = new sf::View();
        this->MainViewRect.height = this->window->getSize().y;
        this->MainViewRect.width = this->window->getSize().x;
        this->window->setView(*this->MainView);
        this->gameMap->MainView = this->MainView;
        this->MainView->reset(this->MainViewRect);
    //Set the default view up
        this->DefaultView = new sf::View(sf::Vector2f(0, 0), sf::Vector2f(this->window->getSize().x,this->window->getSize().y));
    //Start the FPS Timer
        this->FPS_Counter = 0;
        this->FPS_Cache = 0;
        this->FPS = 0;
        this->FrameClock.restart();

    //Load the Map


    while(CConfig::Get()->isRunning())
    {
        this->onEvent();
        this->onUpdate();
        this->onRender();
    }
    this->onExit();
    return true;
}
