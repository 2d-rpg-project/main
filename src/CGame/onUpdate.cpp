#include <CGame.h>
#include <iostream>
#include <CConfig.h>
#include <sstream>
using namespace std;

void CGame::onUpdate()
{
    this->FPS_Counter++;
    this->FrameTime = this->FrameClock.getElapsedTime().asSeconds();
    this->FrameClock.restart();
    this->FPS_Cache += this->FrameTime;
    if(this->FPS_Counter > 30)
    {
        this->FPS = 1 / (this->FPS_Cache / 30);
        this->FPS_Cache = 0;
        this->FPS_Counter = 0;
    }
    if(CConfig::Get()->getDebug(DebugMode::master))
    {
        stringstream DebugStream;
        DebugStream << "FPS: " << (int) this->FPS << " - 1 Frame (ms): " << this->FrameTime * 1000 << endl;
        DebugStream << "Visible Chunks: " << this->gameMap->getVisibleChunks() << endl;
        DebugStream << "Map-State: " << this->gameMap->getCurrentState() << endl;
        this->DebugText->setString(DebugStream.str());
    }
    if(CConfig::Get()->getGameState() == GameState::game)
    {
        this->gameMap->update(this->MainViewRect);
    }
}
