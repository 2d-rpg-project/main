#include <iostream>

#include <CGame.h>
#include <CConfig.h>

using namespace std;

void CGame::onRender()
{
    CConfig::Get()->lockWindowMutex();
    //Set the Window active
        this->window->setActive(true);
    //Clear the window
        this->window->clear();
    //Render the game
    switch(CConfig::Get()->getGameState())
    {
        case GameState::game:
            this->window->setView(*this->MainView);
            this->gameMap->draw(this->window, false);
            this->gameMap->draw(this->window, true);
            this->window->setView(*this->DefaultView);
            break;
        default:
            cout << "!! Unknown Game-State!" << endl;
            break;
    }
    if(CConfig::Get()->getDebug(DebugMode::master))
    {
        if(this->DebugText != NULL)
            this->window->draw(*this->DebugText);
    }
    //Show refresh the window
        this->window->display();
    //Set the Window inactive
        this->window->setActive(false);
    CConfig::Get()->unlockWindowMutex();
}
