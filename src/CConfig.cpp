#include "CConfig.h"

CConfig::CConfig()
{
    this->running = true;
    this->gameState = GameState::def;
}

CConfig::~CConfig()
{
    //dtor
}
bool CConfig::getDebug(unsigned int debugMode)
{
    for(unsigned int i = 0; i < this->debug.size(); i++)
    {
        if(this->debug[i]->type == debugMode)
        {
            return this->debug[i]->state;
        }
    }
    return false;
}
void CConfig::setDebug(unsigned int debugMode, bool state)
{
    for(unsigned int i = 0; i < this->debug.size(); i++)
    {
        if(this->debug[i]->type == debugMode)
        {
            this->debug[i]->state = state;
            return;
        }
    }
    s_debugMode *newDebugMode = new s_debugMode;
    newDebugMode->state = state;
    newDebugMode->type = debugMode;
    this->debug.push_back(newDebugMode);
}
void CConfig::deleteDebug(unsigned int debugMode)
{
    for(unsigned int i = 0; i < this->debug.size(); i++)
    {
        if(this->debug[i]->type == debugMode)
        {
            delete this->debug[i];
            this->debug.erase(this->debug.begin() + i);
        }
    }
}
void CConfig::resetDebug()
{
    for(unsigned int i = 0; i < this->debug.size(); i++)
    {
        delete this->debug[i];
    }
    this->debug.clear();
}
