#ifndef CMAPLOADER_H
#define CMAPLOADER_H

#include <CMap.h>
#include <string>
#include <boost/thread.hpp>
#include <boost/signal.hpp>
#include <angelscript.h>
class CMapLoader
{
    friend class CMap;
    public:
        CMapLoader(CMap *gameMap, sf::RenderWindow *window);
        void setGameMap(CMap *gameMap);
        bool loadFromFile(std::string filename);
        bool loadFromString(std::string mapString);
        virtual ~CMapLoader();
        std::string getTextureName(int layer);
        float getProgressPercent();
        unsigned long int getTileCount();
        unsigned long int getCurrentTile();
        unsigned int getLayerCount();
        bool isFinished();
        std::string getCurrentStatus();
        boost::signal<void (CMapLoader*)>* getOnLoadedEvent();
    protected:
        /**
         * @brief This signal is thrown when the map loader has finished loading the map.
         */
        boost::signal<void (CMapLoader*)> onFinished;
    private:
        CMap *gameMap;
        sf::RenderWindow *window;
        unsigned long int tileCount;
        unsigned long int currentTile;
        bool finished;
        std::string currentStatus;
        boost::mutex getMutex;
        void setCurrentStatus(std::string status);
        void load_V02(tinyxml2::XMLDocument &doc);
};

class CMapLoaderException
{
    public:
        CMapLoaderException(const std::string& msg) : _msg(msg) {}
        ~CMapLoaderException() {}
        std::string what() const {return _msg;}
    private:
        std::string _msg;
};

#endif // CMAPLOADER_H
