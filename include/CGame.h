#ifndef CGAME_H
#define CGAME_H

#include <SFML/Graphics.hpp>

#include <CMap.h>
class CGame
{
    public:
        CGame();
        virtual ~CGame();
        bool onInit();
        void onEvent();
        void onUpdate();
        void onRender();
        void onExit();
    protected:
    private:
        sf::RenderWindow *window;
        //Debugtext
            sf::Text *DebugText;
        //Map
            CMap *gameMap;
        //View
            sf::View *MainView;
            sf::View *DefaultView;
            sf::FloatRect MainViewRect;
        //Frame Clock (FPS)
            sf::Clock FrameClock;
            float FrameTime;
            float FPS;
            float FPS_Cache;
            int FPS_Counter;
};

#endif // CGAME_H
