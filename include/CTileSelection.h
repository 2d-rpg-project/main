#ifndef CTILESELECTION_H
#define CTILESELECTION_H

#include <SFML/Graphics.hpp>
#include <vector>
#include <CMapTile.h>

class CTileSelection
{
    public:
        CTileSelection(sf::Vector2i size, sf::Vector2f pos);
        virtual ~CTileSelection();
        void setPosition(sf::Vector2f pos);
        void addTile(CMapTile *tile, sf::Vector2i gridPos);
        void setTextures(sf::Texture *layer0, sf::Texture *layer1, sf::Texture *layer2);
    protected:
        sf::Texture *layer0;
        sf::Texture *layer1;
        sf::Texture *layer2;
        struct selectionTile {
            sf::Vector2i gridPos;
            sf::Sprite sprite;
            sf::RectangleShape box;
            CMapTile *tile;
        };
        virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;
        sf::Vector2f pos;
        std::vector<selectionTile> tiles;
    private:
};

#endif // CTILESELECTION_H
