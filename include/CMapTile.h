#ifndef CMAPTILE_H
#define CMAPTILE_H

#include <SFML/Graphics.hpp>

class CMapTile
{
    public:
        CMapTile(sf::Vector2i pos, sf::Vector2i tex, unsigned int layer, unsigned int rotation);
        virtual ~CMapTile();
        sf::Vector2i pos;
        sf::Vector2i tex;
        unsigned int layer;
        unsigned int rotation;
    protected:
    private:
};

#endif // CMAPTILE_H
