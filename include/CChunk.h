#ifndef CCHUNK_H
#define CCHUNK_H

#include <SFML/Graphics.hpp>
#include <CMapTile.h>

struct SChunkLayer {
    sf::Texture *texture;
    bool visible;
    bool foreground;
    sf::VertexArray vertices;
    CMapTile *tiles[16][16];
};

class CChunk : public sf::Drawable
{
    public:
        CChunk();
        void setPos(sf::Vector2i pos);
        void setTexture(unsigned int layer, sf::Texture *texture);
        void setVertex(CMapTile *tile);
        void setTile(CMapTile *tile, bool reRender = false);
        void deleteTile(sf::Vector2i tile, unsigned int layer);
        void setVisibleLayer(unsigned int layer, bool visible);
        void setLayer(unsigned int layer, bool foreground, bool visible, sf::Texture *texture);
        CMapTile* getTile(sf::Vector2i, unsigned int layer);
        void reRender();
        sf::FloatRect getRectangle();
        virtual ~CChunk();
        bool visible;
        void clear();
        std::string getFullXML();
        void drawForeground(sf::RenderTarget &target, sf::RenderStates states);
        void directReset();
        void setLayerCount(unsigned int count);
    protected:
        virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;
        sf::Vector2i pos;
        std::vector<SChunkLayer> layer;
    private:
};

#endif // CCHUNK_H
