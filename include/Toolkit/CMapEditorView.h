#ifndef CMAPEDITORVIEW_H
#define CMAPEDITORVIEW_H

#include <wxSFMLCanvas.hpp>
#include <CMap.h>
#include <string>

class CMapEditorView : public wxSFMLCanvas
{
    public:
        CMapEditorView(wxWindow* Parent, const wxSize& Size, wxWindowID Id, const wxPoint& Position, long Style);
        virtual ~CMapEditorView();
        void loadMap(std::string file);
        void saveMap(std::string file, bool compress);
    protected:
    private:
        CMap *gameMap;
        sf::RectangleShape shape;
        virtual void onEvent(sf::Event &event);
        virtual void onUpdate();
        virtual void onRender();

        sf::Vector2i relativeMousePos;

        //void drawSelection();

        //Events
            virtual void onLeftMousePressed(wxMouseEvent &event);
            virtual void onMouseMoved(wxMouseEvent &event);
        //View
            sf::View *MainView;
            sf::FloatRect MainViewRect;
};

#endif // CMAPEDITORVIEW_H
