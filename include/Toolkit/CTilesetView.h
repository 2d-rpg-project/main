#ifndef CTILESETVIEW_H
#define CTILESETVIEW_H

#include <wxSFMLCanvas.hpp>

class CTilesetView : public wxSFMLCanvas
{
    public:
        CTilesetView(wxWindow* Parent, const wxSize& Size, wxWindowID Id, const wxPoint& Position, long Style);
        virtual ~CTilesetView();
    protected:
        virtual void onUpdate();
        virtual void onRender();
    private:
};

#endif // CTILESETVIEW_H
