#ifndef CMAPEDITOR_H
#define CMAPEDITOR_H

#include <vector>
#include <FL/Fl_Box.H>

class CMapEditor : public Fl_Box
{
    public:
        CMapEditor(int x, int y, int w, int h);
        virtual ~CMapEditor();
        virtual void draw();
    protected:
    private:
};

#endif // CMAPEDITOR_H
