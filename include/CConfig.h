#ifndef CCONFIG_H
#define CCONFIG_H

#include <CSingleton.h>
#include <vector>
#include <boost/thread.hpp>
namespace DebugMode {
    enum DebugMode {
        master,
        textureManager,
        fontManager
    };
}
namespace GameState {
    enum GameState {
        def,
        game
    };
}


class CConfig : public TSingleton<CConfig>
{
    public:
        CConfig();
        virtual ~CConfig();
        int getGameState() {return gameState;}
        bool isRunning() {return running;}
        bool getDebug(unsigned int debugMode);
        void setDebug(unsigned int debugMode, bool state = true);
        void setGameState(int state) {gameState = state;}
        void deleteDebug(unsigned int debugMode);
        void resetDebug();
        void quitGame() {running = false;}
        void lockWindowMutex() {windowMutex.lock();}
        void unlockWindowMutex() {windowMutex.unlock();}
    protected:
    private:
        struct s_debugMode
        {
            unsigned int type;
            bool state;
        };
        //Technical Game Variables
            bool running;
            int gameState;
        //Debug Variables
            std::vector<s_debugMode*> debug;
        //Window Mutex
            boost::mutex windowMutex;
};

#endif // CCONFIG_H
