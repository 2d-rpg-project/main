#ifndef CMAP_H
#define CMAP_H

#include <string>
#include <vector>
#include <thread>
#include <mutex>
#include <tinyxml2.h>
#include <SFML/Graphics.hpp>
#include <CChunk.h>
#include <boost/thread.hpp>
#include <boost/signal.hpp>
class CMapLoader;

struct SMapLayer {
    std::string textureName;
    bool visible;
    bool foreground;
};

class CMap
{
    friend class CMapLoader;
    public:
        CMap(sf::RenderWindow *window);
        virtual ~CMap();
        void loadAsync(std::string filename, sf::RenderWindow *window);
        void loadStringAsync(std::string mapString, sf::RenderWindow *window);
        void clearChunks();
        sf::View *MainView;
        void update(sf::FloatRect &MainViewRect);
        unsigned int getVisibleChunks(){return visibleChunks;}
        void setTile(CMapTile *tile, bool reRender);
        CMapTile* getTile(sf::Vector2i pos, unsigned int layer);
        sf::Vector2i getSize();
        void reRender(sf::Vector2i chunk);
        void reRenderAll();
        void reRenderAllVisible();
        void saveToFile(std::string filename, bool compress);
        std::string saveToString();
        void setDrawUnsetTiles(bool state) {drawUnsetTiles = state;}
        void setSize(sf::Vector2i newSize);
        void setSize(int X, int Y) {setSize(sf::Vector2i(X, Y));}
        void setTitle(std::string title);
        void setAuthor(std::string author);
        void setTileset(unsigned int layer, std::string textureName, bool foreground);
        void deleteTile(sf::Vector2i tile, int layer);
        std::string getAuthor();
        std::string getTitle();
        std::string getTileset(unsigned int layer);
        void reAlignChunks();
        boost::mutex mapMutex;
        std::string getCurrentState();
        void draw(sf::RenderTarget *target, bool foreground);
        boost::signal<void (CMapLoader *)>* getOnLoadedEvent();
        bool getVisibleLayer(unsigned int layer);
        void setVisibleLayer(int layer, bool visible);
        bool getDrawUnsetTiles();
        unsigned int getLayerCount();
        bool getForeground(unsigned int layer);
    protected:
        boost::mutex getSetMutex;
        sf::Vector2i convertToChunk(sf::Vector2i tilePos);
        bool drawUnsetTiles;
        sf::RectangleShape *unsetSpaceShape;
        std::vector<std::vector<CChunk> > chunks;
        unsigned int visibleChunks;
        std::string mapAuthor, mapTitle;
        std::vector<SMapLayer> mapLayers;
        unsigned int sizeX, sizeY;
    private:
        CMapLoader *mapLoader;
        sf::RenderWindow *window;
};

#endif // CMAP_H
