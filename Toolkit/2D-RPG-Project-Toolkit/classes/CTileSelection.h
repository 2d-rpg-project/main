#ifndef CTILESELECTION_H
#define CTILESELECTION_H

#include <vector>
#include <iostream>
#include <memory>

#include <SFML/Graphics.hpp>
#include <boost/thread.hpp>
class CSelectionSource;
class CSelectionTarget;
class CTileSelectionTile;

enum class FillMethod {
    CREATE,
    REMOVE,
    REPLACE,
    REPLACEALL
};

class CTileSelection : public sf::Drawable
{
public:
    CTileSelection(CSelectionSource *source, sf::Vector2i pos);
    CTileSelection(CTileSelection& sel);
    ~CTileSelection();
    void update(sf::Vector2i mousePos);
    void setPosition(sf::Vector2i pos);
    CTileSelectionTile* getSelectionTile(unsigned int x, unsigned int y, unsigned int z);
    sf::Vector2i getSize();
    sf::Vector2i getPosition();
    CSelectionSource* getSource();
    void apply(CSelectionTarget *target, FillMethod method, sf::Vector2i pos);
protected:
    virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;
    std::vector<std::vector<std::vector<std::shared_ptr<CTileSelectionTile> > > > tiles;
    sf::Vector2i pos;
    sf::Vector2i selectionPos;
    sf::Vector2i size;
    CSelectionSource *source;
    //Threading Security
        boost::mutex getSetMutex;
};

#endif // CTILESELECTION_H
