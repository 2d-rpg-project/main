#ifndef CMAPEDITORVIEW_H
#define CMAPEDITORVIEW_H

#include <QString>
#include <classes/CSFMLWidget.h>
#include <CChunk.h>
#include <CMap.h>
#include <iostream>
#include <boost/signal.hpp>
#include <classes/CSelectionTarget.h>
#include <classes/CTileSelection.h>

class CTilesetView;

class CMapEditorView : public CSFMLWidget, public CSelectionTarget
{
public:
    CMapEditorView(QWidget* Parent, const QPoint& Position, const QSize& Size, unsigned int FrameTime) :
        CSFMLWidget(Parent, Position, Size, FrameTime)
    {
        this->gameMap = NULL;
        this->MainView = NULL;
        this->tilesetView = NULL;
        //Init Game-Map
            this->MainView = new sf::View();
            this->gameMap = new CMap(this);
            this->gameMap->MainView = this->MainView;
    }
    void loadMap(QString filename);
    void saveMap(QString filename, bool compress);
    virtual ~CMapEditorView();
    void rotateTile(sf::Vector2i tile, unsigned int layer);
    unsigned int getWorkingLayer() {return workingLayer;}
    void setWorkingLayer(unsigned int layer);
    CMap* getMapP() {return gameMap;}
    boost::signal<void (CMapLoader *)> *getOnLoadedEvent();
    void connectTilesetView(CTilesetView *tilesetView);
    CTilesetView* getTilesetView();
    void updateTileSelection(CTileSelection *selection);
    //CSelectionTarget-Derived
        virtual void deleteTileAt(sf::Vector2i pos, unsigned int layer);
        virtual void setMapTile(CMapTile *tile);
private:
    virtual void onUpdate();
    virtual void onEvent(sf::Event &event);
    virtual void onInit();
    virtual void onExit();
    virtual void onResize(sf::Vector2f newSize);
    //Working Layer
        unsigned int workingLayer;
    //Tileset View
        CTilesetView *tilesetView;
    //Mouse Position
        sf::Vector2i mousePosition;
        sf::Vector2i oldMousePosition;
    //Map
        CMap *gameMap;
    //View
        sf::View *MainView;
        sf::FloatRect MainViewRect;
        float zoomFactor;
        float zoomFactorInv;
    //Scrolling Cache Variable
        bool scrollingCache;
    //Cursor
        QCursor widget_cursor;
    //Tileselection
        CTileSelection* tileSelection;
};

#endif // CMAPEDITORVIEW_H
