#include "CTileSelection.h"

#include <classes/CSelectionSource.h>
#include <classes/CSelectionTarget.h>
#include <classes/CTileSelectionTile.h>

using namespace std;

CTileSelection::CTileSelection(CSelectionSource *source, sf::Vector2i pos)
{
    this->pos = pos;
    this->source = source;
}

CTileSelection::~CTileSelection()
{
    this->tiles.clear();
}
CTileSelection::CTileSelection(CTileSelection &sel)
{
    this->pos = sel.getPosition();
    this->source = sel.getSource();
    this->update(sf::Vector2i(sel.getSize().x * 32 + pos.x, sel.getSize().y * 32 + pos.y));
}

sf::Vector2i CTileSelection::getPosition()
{
    this->getSetMutex.lock();
    sf::Vector2i pos = this->pos;
    this->getSetMutex.unlock();
    return pos;
}
CSelectionSource* CTileSelection::getSource()
{
    this->getSetMutex.lock();
    CSelectionSource *selSource = this->source;
    this->getSetMutex.unlock();
    return selSource;
}

void CTileSelection::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    unsigned int x, y, z;
    for(x = 0; x < this->tiles.size(); x++)
    {
        for(y = 0; y < this->tiles[x].size(); y++)
        {
            for(z = 0; z < this->tiles[x][y].size(); z++)
            {
                if(this->tiles[x][y][z].get() != NULL)
                    target.draw(*this->tiles[x][y][z].get(), states);
            }
        }
    }
}
void CTileSelection::update(sf::Vector2i mousePos)
{
    this->getSetMutex.lock();
    this->selectionPos = mousePos;
    unsigned int x, y, z;
    this->size.x = abs(this->selectionPos.x / 32 - this->pos.x / 32); //Always positive
    this->size.y = abs(this->selectionPos.y / 32 - this->pos.y / 32);
    this->tiles.resize(this->size.x);
    for(x = 0; x < this->tiles.size(); x++)
    {
        this->tiles[x].resize(this->size.y);
        for(y = 0; y < this->tiles[x].size(); y++)
        {
            if(this->tiles[x][y].size() != 3)
                this->tiles[x][y].resize(3);
            for(z = 0; z < this->tiles[x][y].size(); z++)
            {
                if(this->source->getTileAt(x + this->pos.x / 32, y + this->pos.y / 32, z) != NULL && this->source->getTexturePtr(z) != nullptr)
                {
                    std::shared_ptr<CTileSelectionTile> tile(new CTileSelectionTile(sf::Vector2i(this->pos.x + 32 * x,
                                                                                                 this->pos.y + 32 * y), z,
                                                                                    this->source->getTexturePtr(z),
                                                                                    this->source->getTileAt(x + this->pos.x / 32, y + this->pos.y / 32, z)->tex));
                    this->tiles[x][y][z] = tile;
                    tile.reset();
                }
            }
        }
    }
    this->getSetMutex.unlock();
}
void CTileSelection::setPosition(sf::Vector2i pos)
{
    unsigned int x, y, z;
    this->getSetMutex.lock();
    this->pos = pos;
    for(x = 0; x < this->tiles.size(); x++)
    {
        for(y = 0; y < this->tiles[x].size(); y++)
        {
            for(z = 0; z < this->tiles[x][y].size(); z++)
            {
                if(this->tiles[x][y][z].get() != NULL)
                    this->tiles[x][y][z]->setPosition(sf::Vector2i(this->pos.x + 32 * x,
                                                                   this->pos.y + 32 * y));
            }
        }
    }
    this->getSetMutex.unlock();
}
sf::Vector2i CTileSelection::getSize()
{
    this->getSetMutex.lock();
    sf::Vector2i size = this->size;
    this->getSetMutex.unlock();
    return size;
}
CTileSelectionTile* CTileSelection::getSelectionTile(unsigned int x, unsigned int y, unsigned int z)
{
    if((unsigned int) this->getSize().x + 1 > x)
    {
        if((unsigned int)this->getSize().y + 1 > y)
        {
            if(this->tiles[x][y].size() > z)
                return this->tiles[x][y][z].get();
        }
    }
    else
        return NULL;
}
void CTileSelection::apply(CSelectionTarget *target, FillMethod method, sf::Vector2i pos)
{
    unsigned int x, y, z;
    for(x = 0; x < this->tiles.size(); x++)
    {
        for(y = 0; y < this->tiles[x].size(); y++)
        {
            for(z = 0; z < this->tiles[x][y].size(); z++)
            {
                if(this->tiles[x][y][z].get() != NULL)
                {
                    if(method == FillMethod::CREATE)
                        target->setMapTile(this->tiles[x][y][z]->toMapTile(sf::Vector2i(pos.x + x, pos.y + y)));
                    if(method == FillMethod::REMOVE)
                        target->deleteTileAt(sf::Vector2i(pos.x + x, pos.y + y), z);
                }
            }
        }
    }
}
