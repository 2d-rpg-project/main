#include "CTileSelectionTile.h"
#include <iostream>
using namespace std;

CTileSelectionTile::CTileSelectionTile(sf::Vector2i pos, unsigned int layer, sf::Texture *tex, sf::Vector2i texPos)
{
    this->sprite = new sf::Sprite(*tex, sf::IntRect(texPos.x, texPos.y, 32, 32));
    this->sprite->setPosition(pos.x, pos.y);
    this->boxShape = new sf::RectangleShape(sf::Vector2f(32, 32));
    this->boxShape->setFillColor(sf::Color(255, 177, 61, 60)); //Orange
    this->boxShape->setOutlineColor(sf::Color(255, 51, 0, 200)); //Darker Orange
    this->boxShape->setOutlineThickness(1);
    this->boxShape->setPosition(pos.x, pos.y);
    this->rotation = 0;
    this->layer = layer;
    this->texPos = texPos;
}
CTileSelectionTile::~CTileSelectionTile()
{
    if(this->sprite != NULL)
        delete this->sprite;
    if(this->boxShape != NULL)
        delete this->boxShape;
}
void CTileSelectionTile::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    if(this->sprite != NULL)
        target.draw(*this->sprite, states);
    if(this->boxShape != NULL)
        target.draw(*this->boxShape, states);
}
CMapTile* CTileSelectionTile::toMapTile(sf::Vector2i mapPos)
{
    CMapTile *tile = new CMapTile(mapPos, texPos, this->layer, this->rotation);
    return tile;
}
void CTileSelectionTile::updateSpriteGeometry()
{
    if(this->rotation == 0)
    {
        this->sprite->setRotation(0);
        this->sprite->setPosition(this->pos.x, this->pos.y);
    }
    if(this->rotation == 1)
    {
        this->sprite->setRotation(90);
        this->sprite->setPosition(this->pos.x + 90, this->pos.y);
    }
    if(this->rotation == 2)
    {
        this->sprite->setRotation(180);
        this->sprite->setPosition(this->pos.x + 90, this->pos.y + 90);
    }
    if(this->rotation == 3)
    {
        this->sprite->setRotation(270);
        this->sprite->setPosition(this->pos.x, this->pos.y + 90);
    }
}

void CTileSelectionTile::setPosition(sf::Vector2i pos)
{
    this->pos = pos;
    this->boxShape->setPosition(sf::Vector2f(pos.x, pos.y));
    this->updateSpriteGeometry();
}
void CTileSelectionTile::setRotation(int rotation)
{
    this->rotation = rotation;
    this->updateSpriteGeometry();
}
