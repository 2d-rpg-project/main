#ifndef CTILESELECTIONTILE_H
#define CTILESELECTIONTILE_H

#include <SFML/Graphics.hpp>
#include <CMapTile.h>

class CTileSelectionTile : public sf::Drawable
{
public:
    CTileSelectionTile(sf::Vector2i pos, unsigned int layer, sf::Texture *tex, sf::Vector2i texPos);
    ~CTileSelectionTile();
    CMapTile* toMapTile(sf::Vector2i mapPos);
    void setPosition(sf::Vector2i pos);
    void setRotation(int rotation);
    void updateSpriteGeometry();
protected:
    virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;
private:
    sf::Vector2i pos;
    unsigned int layer;
    sf::Texture *tex;
    sf::Vector2i texPos;
    sf::Sprite *sprite;
    sf::RectangleShape *boxShape;
    int rotation;
};

#endif // CTILESELECTIONTILE_H
