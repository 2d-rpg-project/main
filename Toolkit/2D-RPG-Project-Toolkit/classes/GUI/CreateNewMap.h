#ifndef CREATENEWMAP_H
#define CREATENEWMAP_H

#include <QDialog>
#include <QListWidgetItem>
#include <CMap.h>
#include <classes/CTilesetView.h>
namespace Ui {
class CreateNewMap;
}

class CreateNewMap : public QDialog
{
    Q_OBJECT
    
public:
    explicit CreateNewMap(QWidget *parent = 0);
    explicit CreateNewMap(CMap *map, QWidget *parent = 0);
    ~CreateNewMap();
    void connectTilesetView(CTilesetView *view);
private Q_SLOTS:
    void on_addLayer_clicked();

    void on_layerListDelete_clicked();

    void on_layerList_itemDoubleClicked(QListWidgetItem *item);

    void on_tilesetPathOpenBrowser_clicked();
private:
    CMap *map;
    CTilesetView *view;
private:
    Ui::CreateNewMap *ui;
};

#endif // CREATENEWMAP_H
