#ifndef LAYERINFO_H
#define LAYERINFO_H

#include <QDialog>
#include <QGraphicsScene>
#include <QAbstractButton>

namespace Ui {
class LayerInfo;
}

class LayerInfo : public QDialog
{
    Q_OBJECT
    
public:
    explicit LayerInfo(QWidget *parent = 0);
    void setPath(QString path);
    ~LayerInfo();
    
private Q_SLOTS:
    void on_buttonBox_clicked(QAbstractButton *button);

private:
    void hideFilestats(bool show);
    Ui::LayerInfo *ui;
    QGraphicsScene *graphicsScene;
};

#endif // LAYERINFO_H
