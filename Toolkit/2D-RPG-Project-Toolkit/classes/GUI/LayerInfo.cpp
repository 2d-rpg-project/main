#include "classes/GUI/LayerInfo.h"
#include "ui_LayerInfo.h"
#include <QFileInfo>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QPixmap>

LayerInfo::LayerInfo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LayerInfo)
{
    ui->setupUi(this);
    this->graphicsScene = new QGraphicsScene();
    this->ui->graphicsView->setScene(this->graphicsScene);
    this->hideFilestats(true);
    this->resize(200, 50);
}

LayerInfo::~LayerInfo()
{
    delete ui;
}

void LayerInfo::setPath(QString path)
{
    this->setWindowTitle(QString("Tileset-Info: ").append(path));
    QFileInfo info(path);
    if(info.exists())
    {
        QPixmap pixmap(path);
        this->graphicsScene->addPixmap(pixmap);
        this->ui->tilesetFilesize->setText(QString::number(info.size()).append(" Byte"));
        this->ui->tilesetHeight->setValue(pixmap.size().height() / 32);
        this->ui->tilesetWidth->setValue(pixmap.size().width()  / 32);
        this->hideFilestats(false);
        this->resize(pixmap.size().width() + 50, pixmap.size().height() + 200);
    }
    this->ui->tilesetPath->setText(path);
}

void LayerInfo::on_buttonBox_clicked(QAbstractButton *button)
{
    this->close();
}
void LayerInfo::hideFilestats(bool show)
{
    this->ui->graphicsView->setHidden(show);
    this->ui->tilesetFilesize->setHidden(show);
    this->ui->tilesetHeight->setHidden(show);
    this->ui->tilesetWidth->setHidden(show);
    this->ui->labelHeight->setHidden(show);
    this->ui->labelSize->setHidden(show);
    this->ui->labelWidth->setHidden(show);
}
