#include "classes/GUI/CreateNewMap.h"
#include "ui_CreateNewMap.h"
#include <QListWidgetItem>
#include <QFileInfo>
#include <classes/GUI/LayerInfo.h>
#include <sstream>
#include <QMessageBox>
#include <QFileDialog>
#include <QDir>

using namespace std;

CreateNewMap::CreateNewMap(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateNewMap)
{
    ui->setupUi(this);
    this->map = NULL;
    this->view = NULL;
}

CreateNewMap::CreateNewMap(CMap* map, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateNewMap)
{
    ui->setupUi(this);
    this->map = map;
    this->view = NULL;
    this->setWindowTitle("Edit Map");
    this->ui->mapAuthor->setText(QString::fromStdString(map->getAuthor()));
    this->ui->mapHeight->setValue(map->getSize().y);
    this->ui->mapWidth->setValue(map->getSize().x);
    this->ui->mapName->setText(QString::fromStdString((map->getTitle())));
    unsigned int layerCount = map->getLayerCount();
    for(unsigned int i = 0; i < layerCount; i++)
    {
        QListWidgetItem *item = new QListWidgetItem();
        item->setText(QString::fromStdString(map->getTileset(i)));
        if(map->getForeground(i))
        {
            item->setIcon(QIcon(":Data/Icons/foreground.png"));
        }
        else
        {
            item->setIcon(QIcon(":Data/Icons/background.png"));
        }
        this->ui->layerList->addItem(item);
    }
}
void CreateNewMap::connectTilesetView(CTilesetView *view)
{
    this->view = view;
}

CreateNewMap::~CreateNewMap()
{
    delete ui;
}

void CreateNewMap::on_addLayer_clicked()
{
    QFileInfo info(this->ui->layerTilesetPath->text());
    if(!info.exists())
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        QString msgBoxText;
        msgBox.setWindowTitle("File doesn't exist!");
        msgBoxText.append("The file named \"").append(this->ui->layerTilesetPath->text()).append("\" does not exist! Are you sure you want to continue?");
        msgBox.setText(msgBoxText);
        msgBox.setStandardButtons(QMessageBox::No | QMessageBox::Yes);
        if(msgBox.exec() == QMessageBox::No)
            return;
    }
    if(this->ui->layerTilesetPath->text() != "")
    {
        QListWidgetItem *item = new QListWidgetItem();
        item->setText(this->ui->layerTilesetPath->text());
        if(this->ui->layerTilesetForeground->isChecked())
        {
            item->setIcon(QIcon(":Data/Icons/foreground.png"));
        }
        else
        {
            item->setIcon(QIcon(":Data/Icons/background.png"));
        }
        this->ui->layerList->addItem(item);
    }
}

void CreateNewMap::on_layerListDelete_clicked()
{
    if(this->ui->layerList->count() > 0)
    {
        delete this->ui->layerList->currentItem();
    }
}

void CreateNewMap::on_layerList_itemDoubleClicked(QListWidgetItem *item)
{
    LayerInfo info(this);
    info.setPath(item->text());
    info.exec();
}

void CreateNewMap::on_tilesetPathOpenBrowser_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Open Tileset"),
                                                     "",
                                                    tr("Picture-Files (*.png *.jpg *.gif)"));
    QDir dir("./");
    this->ui->layerTilesetPath->setText(dir.relativeFilePath(filename));
}
