#ifndef CSFMLWIDGET_H
#define CSFMLWIDGET_H

#include <qmetatype.h>
#include <SFML/Graphics.hpp>
#include <QWidget>
#include <QTimer>
class CSFMLWidget : public sf::RenderWindow, public QWidget
{
public:
    CSFMLWidget(QWidget* Parent, const QPoint& Position, const QSize& Size, unsigned int FrameTime);
    virtual ~CSFMLWidget();
protected:
    QTimer myTimer;
    bool   myInitialized;
private:
    virtual void onInit();
    virtual void onUpdate();
    virtual void onExit();
    virtual void onEvent(sf::Event &event);
    virtual QPaintEngine* paintEngine() const;
    virtual void showEvent(QShowEvent*);
    virtual void paintEvent(QPaintEvent*);
    virtual void resizeEvent(QResizeEvent *resize);
    virtual void onResize(sf::Vector2f newSize);
    virtual void mousePressEvent(QMouseEvent *mouseEvent);
    virtual void mouseMoveEvent(QMouseEvent *mouseEvent);
    virtual void wheelEvent(QWheelEvent *wheelEvent);
};

#endif // CSFMLWIDGET_H
