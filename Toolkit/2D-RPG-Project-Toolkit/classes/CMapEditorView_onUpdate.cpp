#include <classes/CMapEditorView.h>
#include <CConfig.h>

void CMapEditorView::onUpdate()
{
    //Lock the Window-Mutex
        CConfig::Get()->lockWindowMutex();
    //Set the mouse position
        this->oldMousePosition = this->mousePosition;
        this->mousePosition = sf::Mouse::getPosition(*this);
    //Events
        if(this->MainView != NULL)
        {
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::W))
            {
                if(sf::Keyboard::isKeyPressed(sf::Keyboard::LShift) || sf::Keyboard::isKeyPressed(sf::Keyboard::RShift))
                    this->MainViewRect.top -= 30;
                else
                    this->MainViewRect.top -= 5;
                this->MainView->reset(this->MainViewRect);
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::A))
            {
                if(sf::Keyboard::isKeyPressed(sf::Keyboard::LShift) || sf::Keyboard::isKeyPressed(sf::Keyboard::RShift))
                    this->MainViewRect.left -= 30;
                else
                    this->MainViewRect.left -= 5;
                this->MainView->reset(this->MainViewRect);
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::S))
            {
                if(sf::Keyboard::isKeyPressed(sf::Keyboard::LShift) || sf::Keyboard::isKeyPressed(sf::Keyboard::RShift))
                    this->MainViewRect.top += 30;
                else
                    this->MainViewRect.top += 5;
                this->MainView->reset(this->MainViewRect);
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::D))
            {
                if(sf::Keyboard::isKeyPressed(sf::Keyboard::LShift) || sf::Keyboard::isKeyPressed(sf::Keyboard::RShift))
                    this->MainViewRect.left += 30;
                else
                    this->MainViewRect.left += 5;
                this->MainView->reset(this->MainViewRect);
            }
            if(sf::Mouse::isButtonPressed(sf::Mouse::Middle))
            {
                if(!this->scrollingCache)
                {
                    this->widget_cursor.setShape(Qt::ClosedHandCursor);
                    this->setCursor(widget_cursor);
                    this->scrollingCache = true;
                }
                this->MainViewRect.left += (this->oldMousePosition.x - this->mousePosition.x) * this->zoomFactor;
                this->MainViewRect.top += (this->oldMousePosition.y - this->mousePosition.y) * this->zoomFactor;
                this->MainView->reset(this->MainViewRect);
            }
            else
            {
                if(this->scrollingCache)
                {
                    this->widget_cursor.setShape(Qt::ArrowCursor);
                    this->setCursor(this->widget_cursor);
                    this->scrollingCache = false;
                }
            }
        }
    //Unlock the Window-Mutex
        CConfig::Get()->unlockWindowMutex();
    //Update
        if(this->gameMap != NULL)
        {
            this->gameMap->update(this->MainViewRect);
        }
    //Lock the Window-Mutex
        CConfig::Get()->lockWindowMutex();
    //Draw
        if(this->gameMap != NULL)
        {
            if(this->MainView != NULL)
                this->setView(*this->MainView);
            this->gameMap->draw(this, false);
            this->gameMap->draw(this, true);
        }
        if(this->tileSelection != NULL)
            this->draw(*this->tileSelection);
        this->setView(this->getDefaultView());
    // Display on screen
        display();
    //Unlock the Window-Mutex
        CConfig::Get()->unlockWindowMutex();
}
