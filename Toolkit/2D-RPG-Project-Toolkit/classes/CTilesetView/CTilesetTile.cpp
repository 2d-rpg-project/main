#include "classes/CTilesetTile.h"

CTilesetTile::CTilesetTile(sf::Vector2i tile, sf::Texture *texture, int layer)
{
    this->sprite = new sf::Sprite();
    this->layer = layer;
    this->sprite->setPosition(sf::Vector2f(tile.x * 32, tile.y * 32));
    this->sprite->setTexture(*texture);
    this->sprite->setTextureRect(sf::IntRect(this->sprite->getPosition().x, this->sprite->getPosition().y,
                                             32, 32));
}
CTilesetTile::~CTilesetTile()
{
    if(this->sprite != NULL)
        delete this->sprite;
}
void CTilesetTile::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    target.draw(*this->sprite, states);
}
CMapTile* CTilesetTile::toMapTile(sf::Vector2i pos, unsigned int rotation, bool foreground)
{
    CMapTile *tile = new CMapTile(pos, sf::Vector2i(this->sprite->getPosition().x,
                                                    this->sprite->getPosition().y),
                                  layer, rotation);
    return tile;
}
sf::FloatRect CTilesetTile::getRectangle()
{
    return sf::FloatRect(this->sprite->getPosition().x, this->sprite->getPosition().y, 32, 32);
}
