#include <classes/CTilesetView.h>
#include <CConfig.h>
#ifdef Q_WS_X11
    #include <Qt/qx11info_x11.h>
    #include <X11/Xlib.h>
#endif

#include <iostream>
#include <Manager/CTextureManager.h>
#include <QRect>

using namespace std;

CTilesetView::CTilesetView(QWidget* Parent, const QPoint& Position, const QSize& Size, unsigned int FrameTime)
    : CSFMLWidget(Parent, Position, Size, FrameTime)
{
    this->mainView = NULL;
    this->activeLayer = 0;
    this->tileSelection = NULL;
    this->editorView = NULL;
    this->mainView = new sf::View();
}
CTilesetView::~CTilesetView()
{
    if(this->tileSelection != NULL)
        delete this->tileSelection;
    if(this->mainView != NULL)
        delete this->mainView;
    this->clearTiles();
    this->layers.clear();
}

void CTilesetView::loadFromTexture(unsigned int layer, sf::Texture *texture)
{
    if(layer < this->layers.size())
    {
        unsigned int x, y, z;
        this->getSetMutex.lock();
        this->layers[layer].texture = texture;
        this->getSetMutex.unlock();
        this->drawMutex.lock();
        for(x = 0; x < this->layers[layer].tiles.size(); x++)
        {
            for(y = 0; y < this->layers[layer].tiles[x].size(); y++)
            {
                shared_ptr<CMapTile> tile(new CMapTile(sf::Vector2i(x, y), sf::Vector2i(x * 32, y * 32), z, 0));
                this->layers[layer].tiles[x][y] = tile;
                tile.reset();
            }
        }
        this->drawMutex.unlock();
    }
    if(layer = this->layers.size() + 1 || this->layers.size() == 0)
    {
        unsigned int x, y, z;
        STilesetLayer tilesetLayer;
        tilesetLayer.texture = texture;
        for(x = 0; x < tilesetLayer.tiles.size(); x++)
        {
            for(y = 0; y < tilesetLayer.tiles[x].size(); y++)
            {
                shared_ptr<CMapTile> tile(new CMapTile(sf::Vector2i(x, y), sf::Vector2i(x * 32, y * 32), z, 0));
                tilesetLayer.tiles[x][y] = tile;
                tile.reset();
            }
        }
        this->getSetMutex.lock();
        this->drawMutex.lock();
        this->layers.push_back(tilesetLayer);
        this->drawMutex.unlock();
        this->getSetMutex.unlock();
    }
    this->updateVertieces();
}
void CTilesetView::onInit()
{
    this->getSetMutex.lock();
    this->mainViewRect.width = this->getSize().x;
    this->mainViewRect.height = this->getSize().y;
    this->mainViewRect.top = 0;
    this->mainViewRect.left = 0;
    this->getSetMutex.unlock();
}
void CTilesetView::onResize(sf::Vector2f newSize)
{
    this->mainViewRect.width = newSize.x;
    this->mainViewRect.height = newSize.y;
    if(this->mainView != NULL)
        this->mainView->reset(this->mainViewRect);
}
void CTilesetView::setActiveLayer(unsigned int layer)
{
    if(layer < this->layers.size())
    {
        this->activeLayer = layer;
        this->setSize(sf::Vector2u(this->layers[layer].texture->getSize().x, this->layers[layer].texture->getSize().y));
        this->setGeometry(QRect(0, 0, this->layers[layer].texture->getSize().x, this->layers[layer].texture->getSize().y));
        this->onResize(sf::Vector2f(this->layers[layer].texture->getSize().x, this->layers[layer].texture->getSize().y));
        this->drawMutex.lock();
        if(this->tileSelection != NULL)
            delete this->tileSelection;
        this->tileSelection = NULL;
        this->drawMutex.unlock();
    }
}

void CTilesetView::onUpdate()
{
    CConfig::Get()->lockWindowMutex();
    this->setActive(true);
    if(this->mainView != NULL)
    {
        this->mainView->reset(this->mainViewRect);
        this->setView(*this->mainView);
    }
    if(this->activeLayer < this->layers.size())
    {
        sf::RenderStates states;
        this->drawMutex.lock();
        states.texture = this->layers[this->activeLayer].texture;
        this->draw(this->layers[this->activeLayer].tileVerts, states);
        if(this->tileSelection != NULL)
            this->draw(*this->tileSelection);
        this->drawMutex.unlock();
    }
    this->setView(this->getDefaultView());
    this->display();
    this->setActive(false);
    CConfig::Get()->unlockWindowMutex();
}
void CTilesetView::showEvent(QShowEvent *)
{
    if (!myInitialized)
    {
        CConfig::Get()->lockWindowMutex();
        // Under X11, we need to flush the commands sent to the server to ensure that
        // SFML will get an updated view of the windows
        #ifdef Q_WS_X11
            XFlush(QX11Info::display());
        #endif
        //Init the SFML window
        sf_create(this->winId());
        // Let the derived class do its specific stuff
        onInit();
        // Setup the timer to trigger a refresh at specified framerate
        connect(&myTimer, SIGNAL(timeout()), this, SLOT(repaint()));
        myTimer.start();
        myInitialized = true;
        CConfig::Get()->unlockWindowMutex();
    }
}
void CTilesetView::loadFromMapLoader(CMapLoader *loader)
{
    for(unsigned int i = 0; i < loader->getTileCount(); i++)
    {
        this->loadFromTexture(i, &CTextureManager::Get()->getTexture(loader->getTextureName(i)));
    }
}
void CTilesetView::clearTiles()
{
    for(unsigned int z = 0; z < this->layers.size(); z++)
    {
        this->layers[z].tiles.clear();
    }
}
void CTilesetView::setTexture(unsigned int layer, string texture)
{
    this->loadFromTexture(layer, &CTextureManager::Get()->getTexture(texture));
}
void CTilesetView::updateVertieces()
{
    unsigned int x, y, z;
    this->drawMutex.lock();
    for(z = 0; z < this->layers.size(); z++)
    {
        this->layers[z].tileVerts.clear();
        this->layers[z].tileVerts.setPrimitiveType(sf::Quads);
    }
    this->drawMutex.unlock();
    for(z = 0; z < this->layers.size(); z++)
    {
        for(x = 0; x < this->layers[z].tiles.size(); x++)
        {
            for(y = 0; y < this->layers[z].tiles[x].size(); y++)
            {
                if(this->layers[z].tiles[x][y] != NULL)
                {
                    sf::Vertex vert[4];
                    vert[0].position = sf::Vector2f(this->layers[z].tiles[x][y]->pos.x * 32, this->layers[z].tiles[x][y]->pos.y * 32);
                    vert[1].position = sf::Vector2f(this->layers[z].tiles[x][y]->pos.x * 32 + 32, this->layers[z].tiles[x][y]->pos.y * 32);
                    vert[2].position = sf::Vector2f(this->layers[z].tiles[x][y]->pos.x * 32 + 32, this->layers[z].tiles[x][y]->pos.y * 32 + 32);
                    vert[3].position = sf::Vector2f(this->layers[z].tiles[x][y]->pos.x * 32, this->layers[z].tiles[x][y]->pos.y * 32 + 32);

                    vert[0].texCoords = sf::Vector2f(this->layers[z].tiles[x][y]->tex.x, this->layers[z].tiles[x][y]->tex.y);
                    vert[1].texCoords = sf::Vector2f(this->layers[z].tiles[x][y]->tex.x + 32, this->layers[z].tiles[x][y]->tex.y);
                    vert[2].texCoords = sf::Vector2f(this->layers[z].tiles[x][y]->tex.x + 32, this->layers[z].tiles[x][y]->tex.y + 32);
                    vert[3].texCoords = sf::Vector2f(this->layers[z].tiles[x][y]->tex.x, this->layers[z].tiles[x][y]->tex.y + 32);

                    this->drawMutex.lock();
                    this->layers[z].tileVerts.append(vert[0]);
                    this->layers[z].tileVerts.append(vert[1]);
                    this->layers[z].tileVerts.append(vert[2]);
                    this->layers[z].tileVerts.append(vert[3]);
                    this->drawMutex.unlock();
                }
            }
        }
    }

}
CTileSelection* CTilesetView::getTileSelection()
{
    this->getSetMutex.lock();
    CTileSelection* sel = this->tileSelection;
    this->getSetMutex.unlock();
    return sel;
}
void CTilesetView::connectMapEditorView(CMapEditorView *view)
{
    this->getSetMutex.lock();
    this->editorView = view;
    this->getSetMutex.unlock();
}
