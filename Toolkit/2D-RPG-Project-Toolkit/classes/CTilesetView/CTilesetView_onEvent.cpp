#include <classes/CTilesetView.h>
#include <classes/CMapEditorView.h>

void CTilesetView::onEvent(sf::Event &event)
{
    if(event.type == sf::Event::MouseButtonPressed)
    {
        if(event.mouseButton.button == sf::Mouse::Left)
        {
            this->currently_dragging = true;
            if(this->tileSelection != NULL)
                delete this->tileSelection;
            this->tileSelection = new CTileSelection(this, sf::Vector2i((event.mouseButton.x / 32) * 32,
                                                                        (event.mouseButton.y / 32) * 32));
        }
    }
    if(event.type == sf::Event::MouseMoved)
    {
        if(currently_dragging)
        {
            if(this->tileSelection != NULL)
                this->tileSelection->update(sf::Vector2i(event.mouseMove.x + 32,
                                                         event.mouseMove.y + 32)); // + 32 to provide smoother selection
        }
    }
    if(!sf::Mouse::isButtonPressed(sf::Mouse::Left))
    {
        if(currently_dragging)
        {
            if(this->editorView != NULL && this->tileSelection != nullptr)
                this->editorView->updateTileSelection(this->tileSelection);
            this->currently_dragging = false;
        }
    }
}
