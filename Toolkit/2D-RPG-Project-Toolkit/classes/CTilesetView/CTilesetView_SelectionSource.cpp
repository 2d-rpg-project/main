#include <classes/CTilesetView.h>

CMapTile* CTilesetView::getTileAt(sf::Vector2i pos, unsigned int layer)
{
    if(this->activeLayer < this->layers.size())
    {
        if(this->layers[activeLayer].tiles.size() > pos.x)
        {
            if(this->layers[activeLayer].tiles[pos.x].size() > pos.y)
            {
                return this->layers[activeLayer].tiles[pos.x][pos.y].get();
            }
        }
    }
    return NULL;
}
CMapTile *CTilesetView::getTileAt(unsigned int X, unsigned int Y, unsigned int layer)
{
    return this->getTileAt(sf::Vector2i(X, Y), layer);
}
float CTilesetView::getZoomFactor()
{
    return 1; //There is no Zoomfactor yet...
}
sf::Texture* CTilesetView::getTexturePtr(int layer)
{
    this->getSetMutex.lock();
    if(layer == this->activeLayer && activeLayer < this->layers.size())
    {
        this->drawMutex.lock();
        sf::Texture *tex = this->layers[this->activeLayer].texture;
        this->drawMutex.unlock();
        this->getSetMutex.unlock();
        return tex;
    }
    this->getSetMutex.unlock();
    return NULL;
}
