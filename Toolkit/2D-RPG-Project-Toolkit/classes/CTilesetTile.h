#ifndef CTILESETTILE_H
#define CTILESETTILE_H

#include <SFML/Graphics.hpp>
#include <CMapTile.h>

class CTilesetTile : public sf::Drawable
{
public:
    CTilesetTile(sf::Vector2i tile, sf::Texture *texture, int layer);
    ~CTilesetTile();
    /**
     * @brief Convert the tileset-tile to a map-tile.
     * @param pos Position of the map-tile on the map.
     * @param rotation Rotation of the map-tile.
     * @param foreground true = rendered above entities, false = rendered normally
     * @return Standard map-tile to save in the game-map with calculated texture coordinates and layer.
     */
    CMapTile* toMapTile(sf::Vector2i pos, unsigned int rotation, bool foreground);
    /**
     * @brief Change the layer this tileset-tile is applied to.
     * @param layer
     */
    void setLayer(int layer);
    sf::FloatRect getRectangle();
protected:
    virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;
    /**
     * @brief The layer this tileset-tile is applied to.
     */
    int layer;
    /**
     * @brief The ID of the tile in the tilesheet.
     */
    unsigned int tile;
    /**
     * @brief The sprite of the tile.
     */
    sf::Sprite *sprite;
    /**
     * @brief The texture this tile contains.
     */
    sf::Texture *texture;
};

#endif // CTILESETTILE_H
