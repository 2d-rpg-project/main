#include "CMapEditorView.h"
#include <SFML/Graphics.hpp>
#include <Manager/CTextureManager.h>
#include <Manager/CFontManager.h>
#include <CConfig.h>
#include <CMap.h>
#include <iostream>
#include <classes/CTilesetView.h>
using namespace std;

CMapEditorView::~CMapEditorView()
{
    this->onExit();
}
void CMapEditorView::onInit()
{
    //Set the texture-manager to the current window
        CTextureManager::Get()->setRenderWindow(this);
    //Init the standard FontManager Fonts
        CFontManager::Get()->createFont("Data/Fonts/Beeb Mode One.ttf", "Beeb Mode One");
        CFontManager::Get()->createFont("Data/Fonts/Inconsolata.otf", "Inconsolata");

    //Set the Debug Mode
        CConfig::Get()->setDebug(DebugMode::master, true);

    //Set Game-State to GameState::Game
        CConfig::Get()->setGameState(GameState::game);

    //Set the Main View up
        this->MainViewRect.height = this->getSize().y;
        this->MainViewRect.width = this->getSize().x;
        this->setView(*this->MainView);
        this->gameMap->MainView = this->MainView;
        this->MainView->reset(this->MainViewRect);
    //Set up the tileselection
        this->tileSelection = NULL;
    //Set the default zoom-factor to 1
        this->zoomFactor = 1;
        this->zoomFactorInv = 1;
}
void CMapEditorView::onExit()
{
    cout << "Deleting the map \"" << this->gameMap->getTitle() << "\" ... ";
    if(this->gameMap != NULL)
        delete this->gameMap;
    if(this->tileSelection != NULL)
        delete this->tileSelection;
    this->gameMap = NULL;
    if(this->MainView != NULL)
        delete this->MainView;
    this->MainView = NULL;
    cout << " ...finished!" << endl;
}
void CMapEditorView::onResize(sf::Vector2f newSize)
{
    this->setSize(sf::Vector2u(newSize.x, newSize.y));
    this->MainViewRect.height = newSize.y * this->zoomFactor;
    this->MainViewRect.width = newSize.x * this->zoomFactor;
    if(this->MainView != NULL)
        this->MainView->reset(this->MainViewRect);
    if(this->gameMap != NULL)
        this->gameMap->reRenderAll();
}
void CMapEditorView::loadMap(QString filename)
{
    if(filename != "" && this->gameMap != NULL)
        this->gameMap->loadAsync(filename.toStdString(), this);
}
void CMapEditorView::saveMap(QString filename, bool compress)
{
    if(filename != "" && this->gameMap != NULL)
        this->gameMap->saveToFile(filename.toStdString(), compress);
}

void CMapEditorView::rotateTile(sf::Vector2i tile, unsigned int layer)
{
    CMapTile *mapTile = this->gameMap->getTile(tile, layer);
    if(mapTile != NULL)
    {
        mapTile->rotation++;
        if(mapTile->rotation > 3)
            mapTile->rotation = 0;
        mapTile->layer = layer;
        this->gameMap->reRenderAllVisible();
    }
}
boost::signal<void (CMapLoader *)>* CMapEditorView::getOnLoadedEvent()
{
    if(this->gameMap != NULL)
    {
        return this->gameMap->getOnLoadedEvent();
    }
}
void CMapEditorView::connectTilesetView(CTilesetView *tilesetView)
{
    this->tilesetView = tilesetView;
    tilesetView->connectMapEditorView(this);
}
void CMapEditorView::setWorkingLayer(unsigned int layer)
{
    this->workingLayer = layer;
    if(this->tilesetView != NULL)
        this->tilesetView->setActiveLayer(layer);
}
CTilesetView* CMapEditorView::getTilesetView()
{
    return this->tilesetView;
}
void CMapEditorView::updateTileSelection(CTileSelection *selection)
{
    if(this->tileSelection != NULL)
        delete this->tileSelection;
    this->tileSelection = new CTileSelection(*selection);
    this->tileSelection->setPosition(sf::Vector2i((int) this->mousePosition.x / 32 * zoomFactor * 32 * zoomFactorInv,
                                                  (int) this->mousePosition.y / 32 * zoomFactor * 32 * zoomFactorInv));
}
void CMapEditorView::setMapTile(CMapTile *tile)
{
    this->gameMap->setTile(tile, true);
}

void CMapEditorView::deleteTileAt(sf::Vector2i pos, unsigned int layer)
{
    if(this->gameMap != NULL)
        this->gameMap->deleteTile(pos, layer);
}
