#include "CSFMLWidget.h"


#include <qmetatype.h>
#include <QResizeEvent>
#include <SFML/Graphics.hpp>
#include <iostream>
#ifdef Q_WS_X11
    #include <Qt/qx11info_x11.h>
    #include <X11/Xlib.h>
#endif

using namespace std;

CSFMLWidget::CSFMLWidget(QWidget* Parent, const QPoint& Position, const QSize& Size, unsigned int FrameTime = 0) :
    QWidget       (Parent),
    myInitialized (false)
{
    // Setup some states to allow direct rendering into the widget
    setAttribute(Qt::WA_PaintOnScreen);
    setAttribute(Qt::WA_OpaquePaintEvent);
    setAttribute(Qt::WA_NoSystemBackground);
    this->setMouseTracking(true);
    // Set strong focus to enable keyboard events to be received
    setFocusPolicy(Qt::StrongFocus);

    // Setup the widget geometry
    move(Position);
    resize(Size);

    // Setup the timer
    myTimer.setInterval(FrameTime);
}
CSFMLWidget::~CSFMLWidget()
{
    this->onExit();
}

void CSFMLWidget::showEvent(QShowEvent*)
{
    if (!myInitialized)
    {
        // Under X11, we need to flush the commands sent to the server to ensure that
        // SFML will get an updated view of the windows
        #ifdef Q_WS_X11
            XFlush(QX11Info::display());
        #endif

        //Init the SFML window
        sf_create(this->winId());

        // Let the derived class do its specific stuff
        onInit();

        // Setup the timer to trigger a refresh at specified framerate
        connect(&myTimer, SIGNAL(timeout()), this, SLOT(repaint()));
        myTimer.start();

        myInitialized = true;
    }
}
QPaintEngine* CSFMLWidget::paintEngine() const
{
    return 0;
}
void CSFMLWidget::paintEvent(QPaintEvent*)
{
    // Clear the Window
    clear(sf::Color(this->palette().background().color().red(), this->palette().background().color().green(), this->palette().background().color().blue()));

    //Get the window events
    sf::Event event;
    while(this->pollEvent(event))
    {
        this->onEvent(event);
    }
    // Let the derived class do its specific stuff
    onUpdate();
}
void CSFMLWidget::onInit()
{

}
void CSFMLWidget::onUpdate()
{

}
void CSFMLWidget::onExit()
{

}
void CSFMLWidget::resizeEvent(QResizeEvent *resize)
{
    this->onResize(sf::Vector2f(resize->size().width(), resize->size().height()));
}
void CSFMLWidget::onResize(sf::Vector2f newSize)
{

}
void CSFMLWidget::onEvent(sf::Event &event)
{

}
void CSFMLWidget::mousePressEvent(QMouseEvent *mouseEvent)
{
    sf::Event event;
    event.type = sf::Event::MouseButtonPressed;
    if(mouseEvent->button() == Qt::LeftButton)
        event.mouseButton.button = sf::Mouse::Left;
    if(mouseEvent->button() == Qt::RightButton)
        event.mouseButton.button = sf::Mouse::Right;
    if(mouseEvent->button() == Qt::MiddleButton)
        event.mouseButton.button = sf::Mouse::Middle;

    event.mouseButton.x = mouseEvent->x();
    event.mouseButton.y = mouseEvent->y();
    this->onEvent(event);
}
void CSFMLWidget::mouseMoveEvent(QMouseEvent *mouseEvent)
{
    sf::Event event;
    event.type = sf::Event::MouseMoved;
    event.mouseMove.x = mouseEvent->x();
    event.mouseMove.y = mouseEvent->y();
    this->onEvent(event);
}
void CSFMLWidget::wheelEvent(QWheelEvent *wheelEvent)
{
    sf::Event event;
    event.type = sf::Event::MouseWheelMoved;
    event.mouseWheel.delta = wheelEvent->delta();
    this->onEvent(event);
}
