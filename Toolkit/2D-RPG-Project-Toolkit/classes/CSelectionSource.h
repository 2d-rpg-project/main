#ifndef CSELECTIONSOURCE_H
#define CSELECTIONSOURCE_H

#include <CMapTile.h>

class CSelectionSource
{
public:
    CSelectionSource();
    virtual ~CSelectionSource();
    virtual sf::Texture* getTexturePtr(int layer) = 0;
    /**
     * @brief Gets the tile at the given position.
     * @param pos Position of the asked tile
     * @param layer Layer of the asked tile
     * @return a Map-Tile (if there is now tile at that position, then a NULL.
     */
    virtual CMapTile* getTileAt(sf::Vector2i pos, unsigned int layer) = 0;
    virtual CMapTile* getTileAt(unsigned int X, unsigned int Y, unsigned int layer) = 0;
    virtual float getZoomFactor() = 0;
};

#endif // CSELECTIONSOURCE_H
