#ifndef CSELECTIONTARGET_H
#define CSELECTIONTARGET_H

#include <SFML/Graphics.hpp>
#include <CMapTile.h>

class CSelectionTarget
{
public:
    CSelectionTarget();
    virtual void setMapTile(CMapTile* tile) = 0;
    virtual void deleteTileAt(sf::Vector2i pos, unsigned int layer) = 0;
};

#endif // CSELECTIONTARGET_H
