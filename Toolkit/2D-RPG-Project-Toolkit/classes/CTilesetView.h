#ifndef CTILESETVIEW_H
#define CTILESETVIEW_H

#include <classes/CSFMLWidget.h>
#include <string>
#include <vector>
#include <boost/thread.hpp>
#include <classes/CTilesetTile.h>
#include <CMapLoader.h>
#include <CMapTile.h>
#include <classes/CSelectionSource.h>
#include <classes/CTileSelection.h>

struct STilesetLayer {
    sf::Texture *texture;
    sf::VertexArray tileVerts;
    std::vector<std::vector<std::shared_ptr<CMapTile> > > tiles;
};

class CMapEditorView;

class CTilesetView : public CSFMLWidget, public CSelectionSource
{
public:
    CTilesetView(QWidget* Parent, const QPoint& Position, const QSize& Size, unsigned int FrameTime);
    virtual ~CTilesetView();

    void loadFromTexture(unsigned int layer, sf::Texture *texture);
    //Get Data
        /**
         * @brief Gets the amount of tiles in the specified layer.
         * @param layer The layer you want to get the tilecount of.
         * @return The amount of tiles there are in the selected layer-sheet.
         **/
        unsigned int getTileCount(int layer);
        /**
         * @brief Returns the size of the tilesheet in tiles.
         * @param layer
         * @return The size of the specified sheet in tiles.
         */
        sf::Vector2i getTilesetSize(int layer);
        void loadFromMapLoader(CMapLoader* loader);
        void clearTiles();
        void setActiveLayer(unsigned int layer);
        void setTexture(unsigned int layer, std::string texture);
        void updateVertieces();
        void connectMapEditorView(CMapEditorView *view);
        CTileSelection* getTileSelection();
    //CSelection Source - Derived overiding
        virtual CMapTile* getTileAt(sf::Vector2i pos, unsigned int layer);
        virtual CMapTile* getTileAt(unsigned int X, unsigned int Y, unsigned int layer);
        virtual sf::Texture* getTexturePtr(int layer);
        virtual float getZoomFactor();
private:
    //Derived functions
        virtual void onInit();
        virtual void onUpdate();
        virtual void onResize(sf::Vector2f newSize);
        virtual void onEvent(sf::Event &event);
        virtual void showEvent(QShowEvent *);
    //Layers
        unsigned int activeLayer;
        std::vector<STilesetLayer> layers;
    //View
        sf::View *mainView;
        sf::FloatRect mainViewRect;
    //Threaded Mutex Stuff
        boost::mutex getSetMutex;
        boost::mutex drawMutex;
    //Tile-Selection
        int currently_dragging;
        CTileSelection *tileSelection;
    //CMapEditorView
        CMapEditorView *editorView;
};

#endif // CTILESETVIEW_H
