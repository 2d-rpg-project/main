#-------------------------------------------------
#
# Project created by QtCreator 2012-11-19T19:19:16
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 2D-RPG-Project-Toolkit
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    classes/CMapEditorView.cpp \
    tinyxml2.cpp \
    classes/CSFMLWidget.cpp \
    SFML/Graphics/Rect.inl \
    ../../src/CConfig.cpp \
    ../../src/CMapTile.cpp \
    ../../src/CChunk.cpp \
    ../../src/CMap/CMap.cpp \
    ../../src/load.cpp \
    CeditorView_Event.cpp \
    classes/CTileSelection.cpp \
    ../../src/CMap/saveMap.cpp \
    classes/CMapEditorView_onUpdate.cpp \
    ../../src/CMap/CMapLoader.cpp \
    ../../src/Manager/CTextureManager.cpp \
    ../../src/Manager/CFontManager.cpp \
    classes/CTilesetView/CTilesetView.cpp \
    classes/CTilesetView/CTilesetTile.cpp \
    classes/CSelectionSource.cpp \
    classes/CSelectionTarget.cpp \
    classes/CTileSelectionTile.cpp \
    classes/CTilesetView/CTilesetView_SelectionSource.cpp \
    classes/CTilesetView/CTilesetView_onEvent.cpp \
    classes/GUI/CreateNewMap.cpp \
    classes/GUI/LayerInfo.cpp \
    ../../Angelscript/source/as_variablescope.cpp \
    ../../Angelscript/source/as_typeinfo.cpp \
    ../../Angelscript/source/as_tokenizer.cpp \
    ../../Angelscript/source/as_thread.cpp \
    ../../Angelscript/source/as_string_util.cpp \
    ../../Angelscript/source/as_string.cpp \
    ../../Angelscript/source/as_scriptobject.cpp \
    ../../Angelscript/source/as_scriptnode.cpp \
    ../../Angelscript/source/as_scriptfunction.cpp \
    ../../Angelscript/source/as_scriptengine.cpp \
    ../../Angelscript/source/as_scriptcode.cpp \
    ../../Angelscript/source/as_restore.cpp \
    ../../Angelscript/source/as_parser.cpp \
    ../../Angelscript/source/as_outputbuffer.cpp \
    ../../Angelscript/source/as_objecttype.cpp \
    ../../Angelscript/source/as_module.cpp \
    ../../Angelscript/source/as_memory.cpp \
    ../../Angelscript/source/as_globalproperty.cpp \
    ../../Angelscript/source/as_generic.cpp \
    ../../Angelscript/source/as_gc.cpp \
    ../../Angelscript/source/as_datatype.cpp \
    ../../Angelscript/source/as_context.cpp \
    ../../Angelscript/source/as_configgroup.cpp \
    ../../Angelscript/source/as_compiler.cpp \
    ../../Angelscript/source/as_callfunc_xenon.cpp \
    ../../Angelscript/source/as_callfunc_x86.cpp \
    ../../Angelscript/source/as_callfunc_x64_msvc.cpp \
    ../../Angelscript/source/as_callfunc_x64_mingw.cpp \
    ../../Angelscript/source/as_callfunc_x64_gcc.cpp \
    ../../Angelscript/source/as_callfunc_sh4.cpp \
    ../../Angelscript/source/as_callfunc_ppc_64.cpp \
    ../../Angelscript/source/as_callfunc_ppc.cpp \
    ../../Angelscript/source/as_callfunc_mips.cpp \
    ../../Angelscript/source/as_callfunc_arm.cpp \
    ../../Angelscript/source/as_callfunc.cpp \
    ../../Angelscript/source/as_bytecode.cpp \
    ../../Angelscript/source/as_builder.cpp \
    ../../Angelscript/source/as_atomic.cpp \
    ../../Angelscript/add_on/scriptstdstring/scriptstdstring_utils.cpp \
    ../../Angelscript/add_on/scriptstdstring/scriptstdstring.cpp \
    ../../Angelscript/add_on/scriptbuilder/scriptbuilder.cpp \
    ../../src/CMap/Loaders/V0.2.cpp \
    ../../Angelscript/add_on/scriptarray/scriptarray.cpp

HEADERS  += mainwindow.h \
    classes/CMapEditorView.h \
    tinyxml2.h \
    SFML/Window.hpp \
    SFML/System.hpp \
    SFML/OpenGL.hpp \
    SFML/Network.hpp \
    SFML/Graphics.hpp \
    SFML/Config.hpp \
    SFML/Audio.hpp \
    classes/CSFMLWidget.h \
    SFML/Graphics/View.hpp \
    SFML/Graphics/VertexArray.hpp \
    SFML/Graphics/Vertex.hpp \
    SFML/Graphics/Transformable.hpp \
    SFML/Graphics/Transform.hpp \
    SFML/Graphics/Texture.hpp \
    SFML/Graphics/Text.hpp \
    SFML/Graphics/Sprite.hpp \
    SFML/Graphics/Shape.hpp \
    SFML/Graphics/Shader.hpp \
    SFML/Graphics/RenderWindow.hpp \
    SFML/Graphics/RenderTexture.hpp \
    SFML/Graphics/RenderTarget.hpp \
    SFML/Graphics/RenderStates.hpp \
    SFML/Graphics/RectangleShape.hpp \
    SFML/Graphics/Rect.hpp \
    SFML/Graphics/PrimitiveType.hpp \
    SFML/Graphics/Image.hpp \
    SFML/Graphics/Glyph.hpp \
    SFML/Graphics/Font.hpp \
    SFML/Graphics/Export.hpp \
    SFML/Graphics/Drawable.hpp \
    SFML/Graphics/ConvexShape.hpp \
    SFML/Graphics/Color.hpp \
    SFML/Graphics/CircleShape.hpp \
    SFML/Window/WindowStyle.hpp \
    SFML/Window/WindowHandle.hpp \
    SFML/Window/Window.hpp \
    SFML/Window/VideoMode.hpp \
    SFML/Window/Mouse.hpp \
    SFML/Window/Keyboard.hpp \
    SFML/Window/Joystick.hpp \
    SFML/Window/GlResource.hpp \
    SFML/Window/Export.hpp \
    SFML/Window/Event.hpp \
    SFML/Window/ContextSettings.hpp \
    SFML/Window/Context.hpp \
    compress_decompress_string.h \
    classes/CTilesetView.h \
    classes/CTileSelection.h \
    ../../sigc++/sigc++.h \
    ../../include/CMap.h \
    ../../include/CMapLoader.h \
    ../../include/Manager/CTextureManager.h \
    ../../include/Manager/CFontManager.h \
    classes/CTilesetView/CTilesetTile.h \
    classes/CTilesetTile.h \
    classes/CSelectionSource.h \
    classes/CSelectionTarget.h \
    classes/CTileSelectionTile.h \
    classes/GUI/CreateNewMap.h \
    classes/GUI/LayerInfo.h \
    ../../include/CChunk.h \
    ../../Angelscript/source/as_variablescope.h \
    ../../Angelscript/source/as_typeinfo.h \
    ../../Angelscript/source/as_tokenizer.h \
    ../../Angelscript/source/as_tokendef.h \
    ../../Angelscript/source/as_thread.h \
    ../../Angelscript/source/as_texts.h \
    ../../Angelscript/source/as_symboltable.h \
    ../../Angelscript/source/as_string_util.h \
    ../../Angelscript/source/as_string.h \
    ../../Angelscript/source/as_scriptobject.h \
    ../../Angelscript/source/as_scriptnode.h \
    ../../Angelscript/source/as_scriptfunction.h \
    ../../Angelscript/source/as_scriptengine.h \
    ../../Angelscript/source/as_scriptcode.h \
    ../../Angelscript/source/as_restore.h \
    ../../Angelscript/source/as_property.h \
    ../../Angelscript/source/as_parser.h \
    ../../Angelscript/source/as_outputbuffer.h \
    ../../Angelscript/source/as_objecttype.h \
    ../../Angelscript/source/as_module.h \
    ../../Angelscript/source/as_memory.h \
    ../../Angelscript/source/as_map.h \
    ../../Angelscript/source/as_generic.h \
    ../../Angelscript/source/as_gc.h \
    ../../Angelscript/source/as_debug.h \
    ../../Angelscript/source/as_datatype.h \
    ../../Angelscript/source/as_criticalsection.h \
    ../../Angelscript/source/as_context.h \
    ../../Angelscript/source/as_configgroup.h \
    ../../Angelscript/source/as_config.h \
    ../../Angelscript/source/as_compiler.h \
    ../../Angelscript/source/as_callfunc_arm_xcode.S \
    ../../Angelscript/source/as_callfunc_arm_gcc.S \
    ../../Angelscript/source/as_callfunc.h \
    ../../Angelscript/source/as_bytecode.h \
    ../../Angelscript/source/as_builder.h \
    ../../Angelscript/source/as_atomic.h \
    ../../Angelscript/source/as_array.h \
    ../../Angelscript/include/angelscript.h \
    ../../Angelscript/add_on/scriptstdstring/scriptstdstring.h \
    ../../Angelscript/add_on/scriptbuilder/scriptbuilder.h \
    ../../Angelscript/add_on/scriptarray/scriptarray.h

INCLUDEPATH += ../../include/ \
INCLUDEPATH += include/ \
INCLUDEPATH += ../../Angelscript/include/ \
win32:INCLUDEPATH += ../../Boost/include/

#LIBS += -lsfml-system -lsfml-window -lsfml-graphics -lsfml-audio

FORMS    += mainwindow.ui \
    classes/GUI/LayerInfo.ui \
    classes/GUI/CreateNewMap.ui

unix:!macx:!symbian:release {
    LIBS += -L$$PWD/../../SFML/Linux64/lib/ -lsfml-audio
    LIBS += -L$$PWD/../../SFML/Linux64/lib/ -lsfml-graphics
    LIBS += -L$$PWD/../../SFML/Linux64/lib/ -lsfml-network
    LIBS += -L$$PWD/../../SFML/Linux64/lib/ -lsfml-window
    LIBS += -L$$PWD/../../SFML/Linux64/lib/ -lsfml-system
    LIBS += -lboost_system-mt
    LIBS += -lboost_thread-mt
    LIBS += -lboost_signals-mt
}
unix:!macx:!symbian:debug {
    LIBS += -L$$PWD/../../SFML/Linux64/lib/ -lsfml-audio
    LIBS += -L$$PWD/../../SFML/Linux64/lib/ -lsfml-graphics
    LIBS += -L$$PWD/../../SFML/Linux64/lib/ -lsfml-network
    LIBS += -L$$PWD/../../SFML/Linux64/lib/ -lsfml-window
    LIBS += -L$$PWD/../../SFML/Linux64/lib/ -lsfml-system
    LIBS += -lboost_system-mt
    LIBS += -lboost_thread-mt
    LIBS += -lboost_signals-mt
}

win32:LIBS += ../../SFML/Windows/lib/libsfml-audio-s.a
win32:LIBS += ../../SFML/Windows/lib/libsfml-network-s.a
win32:LIBS += ../../SFML/Windows/lib/libsfml-graphics-s.a
win32:LIBS += ../../SFML/Windows/lib/libsfml-window-s.a
win32:LIBS += ../../SFML/Windows/lib/libsfml-system-s.a
win32:LIBS += ../../zlib/w32/lib/libz.a
win32:LIBS += ../../zlib/w32/lib/libz.dll.a
win32:LIBS += -static

win32 {
    LIBS += ../../Boost/windows/lib/libboost_system-mgw47-mt-1_52.a
    LIBS += ../../Boost/windows/lib/libboost_thread-mgw47-mt-1_52.a
    LIBS += ../../Boost/windows/lib/libboost_signals-mgw47-mt-1_52.a
}

RESOURCES += application.qrc

CONFIG += exceptions

QMAKE_CXXFLAGS += -std=c++11

CONFIG += no_keywords

DEFINES += BOOST_THREAD_USE_LIB
DEFINES += SFML_STATIC

OTHER_FILES += \
    ../../Angelscript/source/as_callfunc_x64_msvc_asm.asm \
    ../../Angelscript/source/as_callfunc_arm_msvc.asm

