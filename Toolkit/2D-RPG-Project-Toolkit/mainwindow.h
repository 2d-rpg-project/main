#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QPushButton>
#include <QCheckBox>
#include <classes/CMapEditorView.h>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    unsigned int getSelectedWorkingLayer();
    void setViewableLayers();
    /**
     * @brief Updates the tab-texts of the mapTabWidget
     * @param loader Does nothing, just to be compatible with the onLoaded signal.
     */
    void updateMapTitles(CMapLoader *loader);
    void updateMapTitles();
    void updateLayerList(unsigned int index);
private Q_SLOTS:
    void on_actionNew_Map_triggered();

    void on_actionLoad_Map_triggered();

    void on_action_xml_uncompressed_triggered();

    void on_actionEdit_Parameters_triggered();

    void on_mapTabWidget_destroyed();

    void on_mapTabWidget_currentChanged(int index);

    void on_mapTabWidget_tabCloseRequested(int index);

    void on_layerListDoubleclicked(int index);

    void on_options_drawmapSize_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
