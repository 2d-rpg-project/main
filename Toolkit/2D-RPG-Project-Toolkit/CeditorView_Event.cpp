#include <classes/CMapEditorView.h>
#include <iostream>
#include <CConfig.h>
using namespace std;

void CMapEditorView::onEvent(sf::Event &event)
{
    //Lock the Window-Mutex
        CConfig::Get()->lockWindowMutex();
    if(event.type == sf::Event::MouseButtonPressed)
    {
        if(event.mouseButton.button == sf::Mouse::Left)
        {
            if(event.mouseButton.x != 0 && event.mouseButton.y != 0)
            {
                sf::Vector2i tilePos;
                tilePos.x = (int) ((event.mouseButton.x) * this->zoomFactor + this->MainViewRect.left) / 32;
                tilePos.y = (int) ((event.mouseButton.y) * this->zoomFactor + this->MainViewRect.top) / 32;
                if((unsigned int) this->gameMap->getSize().x > (unsigned int)tilePos.x && (unsigned int) this->gameMap->getSize().y > (unsigned int)tilePos.y
                        && this->gameMap->getSize().x != 0
                        && this->gameMap->getSize().y != 0)
                {
                    if(this->tileSelection != NULL)
                        this->tileSelection->apply(this, FillMethod::CREATE, tilePos);
                }
            }
        }
        if(event.mouseButton.button == sf::Mouse::Right)
        {
            sf::Vector2i tilePos;
            tilePos.x = (int) ((event.mouseButton.x) * this->zoomFactor + this->MainViewRect.left) / 32;
            tilePos.y = (int) ((event.mouseButton.y) * this->zoomFactor + this->MainViewRect.top) / 32;
            if((unsigned int) this->gameMap->getSize().x > (unsigned int)tilePos.x && (unsigned int) this->gameMap->getSize().y > (unsigned int)tilePos.y
                    && this->gameMap->getSize().x != 0
                    && this->gameMap->getSize().y != 0)
            {
                if(this->tileSelection != NULL)
                    this->tileSelection->apply(this, FillMethod::REMOVE, tilePos);
            }
        }
    }
    if(event.type == sf::Event::KeyPressed)
    {
        if(event.key.code == sf::Keyboard::R)
        {
            sf::Vector2i tilePos;
            tilePos.x = (int) ((this->mousePosition.x) * this->zoomFactor + this->MainViewRect.left) / 32;
            tilePos.y = (int) ((this->mousePosition.y) * this->zoomFactor + this->MainViewRect.top) / 32;
            if((unsigned int) this->gameMap->getSize().x > (unsigned int)tilePos.x
                    && (unsigned int) this->gameMap->getSize().y > (unsigned int)tilePos.y
                    && this->gameMap->getSize().x != 0
                    && this->gameMap->getSize().y != 0)
            {
                this->rotateTile(tilePos, this->workingLayer);
            }
        }
    }
    if(event.type == sf::Event::MouseWheelMoved)
    {
        if(this->zoomFactor > 0.3)
        {
            if(event.mouseWheel.delta > 0)
            {

                this->zoomFactor = this->zoomFactor - 0.2;
                this->zoomFactorInv = this->zoomFactorInv + 0.2;
                this->MainView->zoom(0.8);
                this->MainViewRect.height = this->getSize().y * this->zoomFactor;
                this->MainViewRect.width = this->getSize().x * this->zoomFactor;
                this->MainView->reset(this->MainViewRect);
            }
        }
        if(event.mouseWheel.delta < 0)
        {
            this->zoomFactor = this->zoomFactor + 0.2;
            this->zoomFactorInv = this->zoomFactorInv - 0.2;
            this->MainView->zoom(1.2);
            this->MainViewRect.height = this->getSize().y * this->zoomFactor;
            this->MainViewRect.width = this->getSize().x * this->zoomFactor;
            this->MainView->reset(this->MainViewRect);
        }
    }
    if(event.type == sf::Event::MouseMoved)
    {
        if(this->tileSelection != NULL)
            this->tileSelection->setPosition(sf::Vector2i((int) (event.mouseMove.x * zoomFactor + this->MainViewRect.left) / 32 * 32,
                                                          (int) (event.mouseMove.y * zoomFactor + this->MainViewRect.top) / 32 * 32));
        if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            sf::Vector2i tilePos;
            tilePos.x = (int) ((this->mousePosition.x) * this->zoomFactor + this->MainViewRect.left) / 32;
            tilePos.y = (int) ((this->mousePosition.y) * this->zoomFactor + this->MainViewRect.top) / 32;
            if(this->tileSelection != NULL)
                this->tileSelection->apply(this, FillMethod::CREATE, tilePos);
        }
    }
    //Unlock the Window-Mutex
        CConfig::Get()->unlockWindowMutex();
}
