#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QFileDialog>
#include <QToolBar>
#include <QPushButton>
#include <QCheckBox>
#include <QScrollArea>
#include <QListWidget>
#include <QIcon>
#include <classes/CMapEditorView.h>
#include <classes/CTilesetView.h>
#include <QDebug>
#include <iostream>
#include <CConfig.h>
#include <classes/GUI/CreateNewMap.h>
#include <classes/GUI/LayerInfo.h>
using namespace std;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    CConfig::Get();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionNew_Map_triggered()
{
    CreateNewMap createMap;
    createMap.exec();
}

void MainWindow::on_actionLoad_Map_triggered()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Open Map"),
                                                     "",
                                                    tr("Map-Files (*.map *.xml)"));
    if(filename != "")
    {
        CMapEditorView *editor = new CMapEditorView(0, QPoint(0, 0), QSize(100, 100), 0);
        this->ui->mapTabWidget->addTab(editor, QString("Loading..."));
        QScrollArea *tilesetViewArea = new QScrollArea(this->ui->TilesetViewAreaContainer);
        CTilesetView *tilesetView = new CTilesetView(tilesetViewArea, QPoint(0, 0), QSize(100, 100), 0);
        tilesetViewArea->setWidget(tilesetView);
        this->ui->TilesetViewAreaContainer->addWidget(tilesetViewArea);
        QListWidget *list = new QListWidget();
        this->ui->layerListContainer->addWidget(list);
        editor->loadMap(filename);
        editor->connectTilesetView(tilesetView);
        editor->getOnLoadedEvent()->connect(boost::bind(&CTilesetView::loadFromMapLoader, tilesetView, _1));
        editor->getOnLoadedEvent()->connect(boost::bind(&MainWindow::updateMapTitles, this, _1));
        connect(list, SIGNAL(list->DoubleClicked()), this, SLOT(on_layerListDoubleclicked()));
    }
}

void MainWindow::on_action_xml_uncompressed_triggered()
{
    QString filename = QFileDialog::getSaveFileName(this, tr("Save map as XML"),
                                                    "",
                                                    tr("Uncompressed Map File (*.xml)"));
    static_cast<CMapEditorView*>(this->ui->mapTabWidget->currentWidget())->saveMap(filename, false);
}

unsigned int MainWindow::getSelectedWorkingLayer()
{

    return 0;
}


void MainWindow::on_actionEdit_Parameters_triggered()
{
    if(this->ui->mapTabWidget->currentWidget() != 0)
    {
        CreateNewMap mapSettingsEditor(static_cast<CMapEditorView*>(this->ui->mapTabWidget->currentWidget())->getMapP(), this);
        if(this->ui->TilesetViewAreaContainer->currentWidget() != NULL)
        {
            mapSettingsEditor.connectTilesetView(static_cast<CTilesetView*>(static_cast<QScrollArea*>(this->ui->TilesetViewAreaContainer->currentWidget())->widget()));
        }
        mapSettingsEditor.exec();
        this->updateMapTitles();
    }
}

void MainWindow::on_mapTabWidget_destroyed()
{

}

void MainWindow::on_mapTabWidget_currentChanged(int index)
{
    if(this->ui->mapTabWidget->widget(index) != NULL)
    {
        if (static_cast<CMapEditorView*>(this->ui->mapTabWidget->widget(index))->getMapP() != NULL)
        {
            if(static_cast<CMapEditorView*>(this->ui->mapTabWidget->widget(index))->getMapP()->getTitle() != "")
                this->ui->mapTabWidget->setTabText(index, QString::fromStdString(static_cast<CMapEditorView*>(this->ui->mapTabWidget->widget(index))->getMapP()->getTitle()));
            else
                this->ui->mapTabWidget->setTabText(index, QString("Loading Map..."));
            this->ui->TilesetViewAreaContainer->setCurrentIndex(index);
            int workingLayer = static_cast<CMapEditorView*>(this->ui->mapTabWidget->widget(index))->getWorkingLayer();
            this->ui->options_drawmapSize->setChecked(static_cast<CMapEditorView*>(this->ui->mapTabWidget->widget(index))->getMapP()->getDrawUnsetTiles());
            this->updateLayerList(index);
        }
    }
}

void MainWindow::on_mapTabWidget_tabCloseRequested(int index)
{
    this->ui->mapTabWidget->widget(index)->deleteLater();
    this->ui->mapTabWidget->removeTab(index);
    this->ui->TilesetViewAreaContainer->widget(index)->deleteLater();
    this->ui->TilesetViewAreaContainer->removeWidget(this->ui->TilesetViewAreaContainer->widget(index));
}

void MainWindow::setViewableLayers()
{
    if(this->ui->mapTabWidget->currentWidget() != NULL)
    {
        if (static_cast<CMapEditorView*>(this->ui->mapTabWidget->currentWidget())->getMapP() != NULL)
        {
            //static_cast<CMapEditorView*>(this->ui->mapTabWidget->currentWidget())->getMapP()->setVisibleLayers(this->ui->Layer1Viewable->isChecked(),
            //                                                                                                 this->ui->Layer2Viewable->isChecked(),
            //                                                                                                 this->ui->Layer3Viewable->isChecked());
        }
    }
}

void MainWindow::updateLayerList(unsigned int index)
{
    if(index < this->ui->layerListContainer->count())
    {
        static_cast<QListWidget*>(this->ui->layerListContainer->widget(index))->clear();
        for(unsigned int i = 0; i < static_cast<CMapEditorView*>(this->ui->mapTabWidget->widget(index))->getMapP()->getLayerCount(); i++)
        {
            QListWidgetItem* item = new QListWidgetItem();
            item->setText(QString::fromStdString(static_cast<CMapEditorView*>(this->ui->mapTabWidget->widget(index))->getMapP()->getTileset(i)));
            if(static_cast<CMapEditorView*>(this->ui->mapTabWidget->widget(index))->getMapP()->getForeground(i))
            {
                item->setIcon(QIcon(":Data/Icons/foreground.png"));
            }
            else
            {
                item->setIcon(QIcon(":Data/Icons/background.png"));
            }
            static_cast<QListWidget*>(this->ui->layerListContainer->widget(index))->addItem(item);
            cout << "Updated Layers" << endl;
        }
    }
}
void MainWindow::on_layerListDoubleclicked(int index)
{
    if(this->ui->layerListContainer->count() > 0)
    {
        LayerInfo info(this);
        info.setPath(static_cast<QListWidget*>(this->ui->layerListContainer->currentWidget())->item(index)->text());
        info.exec();
    }
}

void MainWindow::updateMapTitles()
{
    for(unsigned int i = 0; i < this->ui->mapTabWidget->count(); i++)
    {
        if(this->ui->mapTabWidget->widget(i) != NULL)
        {
            if(static_cast<CMapEditorView*>(this->ui->mapTabWidget->widget(i))->getMapP() != NULL)
            {
                if(static_cast<CMapEditorView*>(this->ui->mapTabWidget->widget(i))->getMapP()->getTitle() != "")
                {
                    this->ui->mapTabWidget->setTabText(i, QString::fromStdString(static_cast<CMapEditorView*>(this->ui->mapTabWidget->widget(i))->getMapP()->getTitle()));
                }
                this->updateLayerList(i);
            }
        }
    }
}

void MainWindow::updateMapTitles(CMapLoader *loader)
{
    this->updateMapTitles();
}
void MainWindow::on_options_drawmapSize_clicked()
{
    if(this->ui->mapTabWidget->currentWidget() != NULL)
    {
        if(static_cast<CMapEditorView*>(this->ui->mapTabWidget->currentWidget())->getMapP() != NULL)
        {
            static_cast<CMapEditorView*>(this->ui->mapTabWidget->currentWidget())->getMapP()->setDrawUnsetTiles(this->ui->options_drawmapSize->isChecked());
        }
    }
}
