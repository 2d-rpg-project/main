/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created: Wed 26. Dec 21:19:49 2012
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../2D-RPG-Project-Toolkit/mainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x08,
      41,   11,   11,   11, 0x08,
      71,   11,   11,   11, 0x08,
     110,   11,   11,   11, 0x08,
     144,   11,   11,   11, 0x08,
     178,   11,   11,   11, 0x08,
     212,   11,   11,   11, 0x08,
     249,   11,   11,   11, 0x08,
     283,  277,   11,   11, 0x08,
     319,  277,   11,   11, 0x08,
     358,   11,   11,   11, 0x08,
     401,  393,   11,   11, 0x08,
     433,  393,   11,   11, 0x08,
     465,  393,   11,   11, 0x08,
     497,   11,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0on_actionNew_Map_triggered()\0"
    "on_actionLoad_Map_triggered()\0"
    "on_action_xml_uncompressed_triggered()\0"
    "on_actionSelectLayer1_triggered()\0"
    "on_actionSelectLayer2_triggered()\0"
    "on_actionSelectLayer3_triggered()\0"
    "on_actionEdit_Parameters_triggered()\0"
    "on_mapTabWidget_destroyed()\0index\0"
    "on_mapTabWidget_currentChanged(int)\0"
    "on_mapTabWidget_tabCloseRequested(int)\0"
    "on_actionFill_Randomly_triggered()\0"
    "checked\0on_Layer1Viewable_clicked(bool)\0"
    "on_Layer2Viewable_clicked(bool)\0"
    "on_Layer3Viewable_clicked(bool)\0"
    "on_options_drawmapSize_clicked()\0"
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->on_actionNew_Map_triggered(); break;
        case 1: _t->on_actionLoad_Map_triggered(); break;
        case 2: _t->on_action_xml_uncompressed_triggered(); break;
        case 3: _t->on_actionSelectLayer1_triggered(); break;
        case 4: _t->on_actionSelectLayer2_triggered(); break;
        case 5: _t->on_actionSelectLayer3_triggered(); break;
        case 6: _t->on_actionEdit_Parameters_triggered(); break;
        case 7: _t->on_mapTabWidget_destroyed(); break;
        case 8: _t->on_mapTabWidget_currentChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->on_mapTabWidget_tabCloseRequested((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->on_actionFill_Randomly_triggered(); break;
        case 11: _t->on_Layer1Viewable_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: _t->on_Layer2Viewable_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 13: _t->on_Layer3Viewable_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 14: _t->on_options_drawmapSize_clicked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MainWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
