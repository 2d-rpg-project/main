/********************************************************************************
** Form generated from reading UI file 'editmapsettings.ui'
**
** Created: Wed 26. Dec 20:55:49 2012
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDITMAPSETTINGS_H
#define UI_EDITMAPSETTINGS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QFormLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QSpinBox>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_EditMapSettings
{
public:
    QVBoxLayout *verticalLayout_2;
    QFormLayout *formLayout;
    QLabel *label;
    QLineEdit *mapName;
    QLabel *label_2;
    QLineEdit *mapAuthor;
    QLabel *label_3;
    QHBoxLayout *horizontalLayout_3;
    QSpinBox *mapSizeX;
    QSpinBox *mapSizeY;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QLineEdit *mapTileset0;
    QLineEdit *mapTileset1;
    QLineEdit *mapTileset2;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *EditMapSettings)
    {
        if (EditMapSettings->objectName().isEmpty())
            EditMapSettings->setObjectName(QString::fromUtf8("EditMapSettings"));
        EditMapSettings->resize(323, 240);
        EditMapSettings->setSizeGripEnabled(false);
        verticalLayout_2 = new QVBoxLayout(EditMapSettings);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        formLayout = new QFormLayout();
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        label = new QLabel(EditMapSettings);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        mapName = new QLineEdit(EditMapSettings);
        mapName->setObjectName(QString::fromUtf8("mapName"));

        formLayout->setWidget(0, QFormLayout::FieldRole, mapName);

        label_2 = new QLabel(EditMapSettings);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        mapAuthor = new QLineEdit(EditMapSettings);
        mapAuthor->setObjectName(QString::fromUtf8("mapAuthor"));

        formLayout->setWidget(1, QFormLayout::FieldRole, mapAuthor);

        label_3 = new QLabel(EditMapSettings);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_3);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        mapSizeX = new QSpinBox(EditMapSettings);
        mapSizeX->setObjectName(QString::fromUtf8("mapSizeX"));
        mapSizeX->setMinimum(1);
        mapSizeX->setMaximum(100000000);

        horizontalLayout_3->addWidget(mapSizeX);

        mapSizeY = new QSpinBox(EditMapSettings);
        mapSizeY->setObjectName(QString::fromUtf8("mapSizeY"));
        mapSizeY->setMinimum(1);
        mapSizeY->setMaximum(100000000);

        horizontalLayout_3->addWidget(mapSizeY);


        formLayout->setLayout(2, QFormLayout::FieldRole, horizontalLayout_3);

        label_4 = new QLabel(EditMapSettings);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_4);

        label_5 = new QLabel(EditMapSettings);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_5);

        label_6 = new QLabel(EditMapSettings);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        formLayout->setWidget(5, QFormLayout::LabelRole, label_6);

        mapTileset0 = new QLineEdit(EditMapSettings);
        mapTileset0->setObjectName(QString::fromUtf8("mapTileset0"));

        formLayout->setWidget(3, QFormLayout::FieldRole, mapTileset0);

        mapTileset1 = new QLineEdit(EditMapSettings);
        mapTileset1->setObjectName(QString::fromUtf8("mapTileset1"));

        formLayout->setWidget(4, QFormLayout::FieldRole, mapTileset1);

        mapTileset2 = new QLineEdit(EditMapSettings);
        mapTileset2->setObjectName(QString::fromUtf8("mapTileset2"));

        formLayout->setWidget(5, QFormLayout::FieldRole, mapTileset2);


        verticalLayout_2->addLayout(formLayout);

        buttonBox = new QDialogButtonBox(EditMapSettings);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout_2->addWidget(buttonBox);


        retranslateUi(EditMapSettings);
        QObject::connect(buttonBox, SIGNAL(accepted()), EditMapSettings, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), EditMapSettings, SLOT(reject()));

        QMetaObject::connectSlotsByName(EditMapSettings);
    } // setupUi

    void retranslateUi(QDialog *EditMapSettings)
    {
        EditMapSettings->setWindowTitle(QApplication::translate("EditMapSettings", "Dialog", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("EditMapSettings", "Name:", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("EditMapSettings", "Author: ", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("EditMapSettings", "Size", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("EditMapSettings", "Tileset (L0):", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("EditMapSettings", "Tileset (L1):", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("EditMapSettings", "Tileset (L2):", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class EditMapSettings: public Ui_EditMapSettings {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDITMAPSETTINGS_H
