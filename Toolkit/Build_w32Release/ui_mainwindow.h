/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Wed 26. Dec 20:55:49 2012
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QSpacerItem>
#include <QtGui/QStackedWidget>
#include <QtGui/QStatusBar>
#include <QtGui/QTabWidget>
#include <QtGui/QToolBar>
#include <QtGui/QTreeView>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionNew_Map;
    QAction *actionLoad_Map;
    QAction *action_map_compressed;
    QAction *action_xml_uncompressed;
    QAction *actionSelectLayer1;
    QAction *actionSelectLayer2;
    QAction *actionSelectLayer3;
    QAction *actionEdit_Parameters;
    QAction *actionFill_Randomly;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QTabWidget *mapTabWidget;
    QFrame *frame_2;
    QVBoxLayout *verticalLayout_6;
    QStackedWidget *TilesetViewAreaContainer;
    QFrame *frame;
    QVBoxLayout *verticalLayout_5;
    QTabWidget *options_tabWidget;
    QWidget *Options_ViewSettingsTab;
    QVBoxLayout *verticalLayout_7;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_8;
    QVBoxLayout *verticalLayout_4;
    QCheckBox *Layer1Viewable;
    QCheckBox *Layer2Viewable;
    QCheckBox *Layer3Viewable;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_10;
    QVBoxLayout *verticalLayout_9;
    QCheckBox *options_drawmapSize;
    QSpacerItem *verticalSpacer;
    QTreeView *treeView;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuSave_Map;
    QMenu *menuMap;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1260, 759);
        actionNew_Map = new QAction(MainWindow);
        actionNew_Map->setObjectName(QString::fromUtf8("actionNew_Map"));
        actionLoad_Map = new QAction(MainWindow);
        actionLoad_Map->setObjectName(QString::fromUtf8("actionLoad_Map"));
        action_map_compressed = new QAction(MainWindow);
        action_map_compressed->setObjectName(QString::fromUtf8("action_map_compressed"));
        action_xml_uncompressed = new QAction(MainWindow);
        action_xml_uncompressed->setObjectName(QString::fromUtf8("action_xml_uncompressed"));
        actionSelectLayer1 = new QAction(MainWindow);
        actionSelectLayer1->setObjectName(QString::fromUtf8("actionSelectLayer1"));
        actionSelectLayer1->setCheckable(true);
        actionSelectLayer1->setChecked(true);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/Data/Icons/layer1.png"), QSize(), QIcon::Normal, QIcon::Off);
        icon.addFile(QString::fromUtf8(":/Data/Icons/layer1.png"), QSize(), QIcon::Active, QIcon::Off);
        icon.addFile(QString::fromUtf8(":/Data/Icons/layer1_active.png"), QSize(), QIcon::Selected, QIcon::Off);
        icon.addFile(QString::fromUtf8(":/Data/Icons/layer1_active.png"), QSize(), QIcon::Selected, QIcon::On);
        actionSelectLayer1->setIcon(icon);
        actionSelectLayer2 = new QAction(MainWindow);
        actionSelectLayer2->setObjectName(QString::fromUtf8("actionSelectLayer2"));
        actionSelectLayer2->setCheckable(true);
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/Data/Icons/layer2.png"), QSize(), QIcon::Normal, QIcon::Off);
        icon1.addFile(QString::fromUtf8(":/Data/Icons/layer2.png"), QSize(), QIcon::Active, QIcon::Off);
        icon1.addFile(QString::fromUtf8(":/Data/Icons/layer2_active.png"), QSize(), QIcon::Selected, QIcon::Off);
        icon1.addFile(QString::fromUtf8(":/Data/Icons/layer2_active.png"), QSize(), QIcon::Selected, QIcon::On);
        actionSelectLayer2->setIcon(icon1);
        actionSelectLayer3 = new QAction(MainWindow);
        actionSelectLayer3->setObjectName(QString::fromUtf8("actionSelectLayer3"));
        actionSelectLayer3->setCheckable(true);
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/Data/Icons/layer3.png"), QSize(), QIcon::Normal, QIcon::Off);
        icon2.addFile(QString::fromUtf8(":/Data/Icons/layer3.png"), QSize(), QIcon::Active, QIcon::Off);
        icon2.addFile(QString::fromUtf8(":/Data/Icons/layer3_active.png"), QSize(), QIcon::Selected, QIcon::Off);
        icon2.addFile(QString::fromUtf8(":/Data/Icons/layer3_active.png"), QSize(), QIcon::Selected, QIcon::On);
        actionSelectLayer3->setIcon(icon2);
        actionEdit_Parameters = new QAction(MainWindow);
        actionEdit_Parameters->setObjectName(QString::fromUtf8("actionEdit_Parameters"));
        actionFill_Randomly = new QAction(MainWindow);
        actionFill_Randomly->setObjectName(QString::fromUtf8("actionFill_Randomly"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        mapTabWidget = new QTabWidget(centralWidget);
        mapTabWidget->setObjectName(QString::fromUtf8("mapTabWidget"));
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(mapTabWidget->sizePolicy().hasHeightForWidth());
        mapTabWidget->setSizePolicy(sizePolicy);
        mapTabWidget->setMinimumSize(QSize(800, 500));
        mapTabWidget->setTabsClosable(true);

        gridLayout->addWidget(mapTabWidget, 0, 1, 1, 1);

        frame_2 = new QFrame(centralWidget);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        sizePolicy.setHeightForWidth(frame_2->sizePolicy().hasHeightForWidth());
        frame_2->setSizePolicy(sizePolicy);
        frame_2->setMinimumSize(QSize(700, 150));
        frame_2->setMaximumSize(QSize(16777215, 250));
        frame_2->setFrameShape(QFrame::NoFrame);
        frame_2->setFrameShadow(QFrame::Sunken);
        frame_2->setLineWidth(0);
        verticalLayout_6 = new QVBoxLayout(frame_2);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        TilesetViewAreaContainer = new QStackedWidget(frame_2);
        TilesetViewAreaContainer->setObjectName(QString::fromUtf8("TilesetViewAreaContainer"));

        verticalLayout_6->addWidget(TilesetViewAreaContainer);


        gridLayout->addWidget(frame_2, 1, 1, 1, 1);

        frame = new QFrame(centralWidget);
        frame->setObjectName(QString::fromUtf8("frame"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy1);
        frame->setMinimumSize(QSize(200, 0));
        frame->setMaximumSize(QSize(150, 16777215));
        frame->setFrameShape(QFrame::NoFrame);
        frame->setFrameShadow(QFrame::Sunken);
        frame->setLineWidth(0);
        verticalLayout_5 = new QVBoxLayout(frame);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        options_tabWidget = new QTabWidget(frame);
        options_tabWidget->setObjectName(QString::fromUtf8("options_tabWidget"));
        options_tabWidget->setEnabled(true);
        options_tabWidget->setLayoutDirection(Qt::LeftToRight);
        Options_ViewSettingsTab = new QWidget();
        Options_ViewSettingsTab->setObjectName(QString::fromUtf8("Options_ViewSettingsTab"));
        verticalLayout_7 = new QVBoxLayout(Options_ViewSettingsTab);
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setContentsMargins(11, 11, 11, 11);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        groupBox_2 = new QGroupBox(Options_ViewSettingsTab);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        sizePolicy1.setHeightForWidth(groupBox_2->sizePolicy().hasHeightForWidth());
        groupBox_2->setSizePolicy(sizePolicy1);
        groupBox_2->setMaximumSize(QSize(16777215, 120));
        verticalLayout_8 = new QVBoxLayout(groupBox_2);
        verticalLayout_8->setSpacing(6);
        verticalLayout_8->setContentsMargins(11, 11, 11, 11);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        Layer1Viewable = new QCheckBox(groupBox_2);
        Layer1Viewable->setObjectName(QString::fromUtf8("Layer1Viewable"));
        Layer1Viewable->setChecked(true);

        verticalLayout_4->addWidget(Layer1Viewable);

        Layer2Viewable = new QCheckBox(groupBox_2);
        Layer2Viewable->setObjectName(QString::fromUtf8("Layer2Viewable"));
        Layer2Viewable->setChecked(true);

        verticalLayout_4->addWidget(Layer2Viewable);

        Layer3Viewable = new QCheckBox(groupBox_2);
        Layer3Viewable->setObjectName(QString::fromUtf8("Layer3Viewable"));
        Layer3Viewable->setChecked(true);

        verticalLayout_4->addWidget(Layer3Viewable);


        verticalLayout_8->addLayout(verticalLayout_4);


        verticalLayout_7->addWidget(groupBox_2);

        groupBox = new QGroupBox(Options_ViewSettingsTab);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        sizePolicy1.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy1);
        groupBox->setMinimumSize(QSize(0, 60));
        groupBox->setMaximumSize(QSize(16777215, 100));
        verticalLayout_10 = new QVBoxLayout(groupBox);
        verticalLayout_10->setSpacing(6);
        verticalLayout_10->setContentsMargins(11, 11, 11, 11);
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setSpacing(6);
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        options_drawmapSize = new QCheckBox(groupBox);
        options_drawmapSize->setObjectName(QString::fromUtf8("options_drawmapSize"));

        verticalLayout_9->addWidget(options_drawmapSize);


        verticalLayout_10->addLayout(verticalLayout_9);


        verticalLayout_7->addWidget(groupBox);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer);

        options_tabWidget->addTab(Options_ViewSettingsTab, QString());

        verticalLayout_5->addWidget(options_tabWidget);


        gridLayout->addWidget(frame, 0, 2, 1, 1);

        treeView = new QTreeView(centralWidget);
        treeView->setObjectName(QString::fromUtf8("treeView"));
        sizePolicy1.setHeightForWidth(treeView->sizePolicy().hasHeightForWidth());
        treeView->setSizePolicy(sizePolicy1);
        treeView->setMinimumSize(QSize(100, 0));
        treeView->setMaximumSize(QSize(200, 16777215));

        gridLayout->addWidget(treeView, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1260, 23));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuSave_Map = new QMenu(menuFile);
        menuSave_Map->setObjectName(QString::fromUtf8("menuSave_Map"));
        menuMap = new QMenu(menuBar);
        menuMap->setObjectName(QString::fromUtf8("menuMap"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        mainToolBar->setStyleSheet(QString::fromUtf8(" QCheckBox {\n"
"     spacing: 8px;\n"
" }\n"
"\n"
" QCheckBox::indicator {\n"
"     width: 16px;\n"
"     height: 16px;\n"
" }\n"
"\n"
" QCheckBox::indicator:unchecked {\n"
"     image: url(:);\n"
" }\n"
"\n"
" QCheckBox::indicator:unchecked:hover {\n"
"     image: url(:);\n"
" }\n"
"\n"
" QCheckBox::indicator:unchecked:pressed {\n"
"     image: url(:);\n"
" }\n"
"\n"
" QCheckBox::indicator:checked {\n"
"     image: url(:);\n"
" }\n"
"\n"
" QCheckBox::indicator:checked:hover {\n"
"     image: url(:);\n"
" }\n"
"\n"
" QCheckBox::indicator:checked:pressed {\n"
"     image: url(:);\n"
" }\n"
"\n"
" QCheckBox::indicator:indeterminate:hover {\n"
"     image: url(:);\n"
" }\n"
"\n"
" QCheckBox::indicator:indeterminate:pressed {\n"
"     image: url(:);\n"
" }"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuMap->menuAction());
        menuFile->addAction(actionNew_Map);
        menuFile->addAction(actionLoad_Map);
        menuFile->addAction(menuSave_Map->menuAction());
        menuSave_Map->addAction(action_map_compressed);
        menuSave_Map->addAction(action_xml_uncompressed);
        menuMap->addAction(actionEdit_Parameters);
        menuMap->addAction(actionFill_Randomly);
        mainToolBar->addAction(actionSelectLayer1);
        mainToolBar->addAction(actionSelectLayer2);
        mainToolBar->addAction(actionSelectLayer3);
        mainToolBar->addSeparator();

        retranslateUi(MainWindow);

        mapTabWidget->setCurrentIndex(-1);
        TilesetViewAreaContainer->setCurrentIndex(-1);
        options_tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        actionNew_Map->setText(QApplication::translate("MainWindow", "New Map", 0, QApplication::UnicodeUTF8));
        actionLoad_Map->setText(QApplication::translate("MainWindow", "Load Map", 0, QApplication::UnicodeUTF8));
        action_map_compressed->setText(QApplication::translate("MainWindow", ".map (compressed)", 0, QApplication::UnicodeUTF8));
        action_xml_uncompressed->setText(QApplication::translate("MainWindow", ".xml (uncompressed)", 0, QApplication::UnicodeUTF8));
        actionSelectLayer1->setText(QApplication::translate("MainWindow", "selectLayer1", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionSelectLayer1->setToolTip(QApplication::translate("MainWindow", "Switch to layer 1", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionSelectLayer1->setShortcut(QApplication::translate("MainWindow", "Ctrl+L, 1", 0, QApplication::UnicodeUTF8));
        actionSelectLayer2->setText(QApplication::translate("MainWindow", "selectLayer2", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionSelectLayer2->setToolTip(QApplication::translate("MainWindow", "Switch to layer 2", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionSelectLayer2->setShortcut(QApplication::translate("MainWindow", "Ctrl+L, 2", 0, QApplication::UnicodeUTF8));
        actionSelectLayer3->setText(QApplication::translate("MainWindow", "selectLayer3", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionSelectLayer3->setToolTip(QApplication::translate("MainWindow", "Switch to layer 3", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionSelectLayer3->setShortcut(QApplication::translate("MainWindow", "Ctrl+L, 3", 0, QApplication::UnicodeUTF8));
        actionEdit_Parameters->setText(QApplication::translate("MainWindow", "Edit Parameters", 0, QApplication::UnicodeUTF8));
        actionFill_Randomly->setText(QApplication::translate("MainWindow", "Fill Randomly", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "Visible Layers", 0, QApplication::UnicodeUTF8));
        Layer1Viewable->setText(QApplication::translate("MainWindow", "Layer 1", 0, QApplication::UnicodeUTF8));
        Layer2Viewable->setText(QApplication::translate("MainWindow", "Layer 2", 0, QApplication::UnicodeUTF8));
        Layer3Viewable->setText(QApplication::translate("MainWindow", "Layer 3", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("MainWindow", "Additional Layers", 0, QApplication::UnicodeUTF8));
        options_drawmapSize->setText(QApplication::translate("MainWindow", "Map-Size", 0, QApplication::UnicodeUTF8));
        options_tabWidget->setTabText(options_tabWidget->indexOf(Options_ViewSettingsTab), QApplication::translate("MainWindow", "View Settings", 0, QApplication::UnicodeUTF8));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", 0, QApplication::UnicodeUTF8));
        menuSave_Map->setTitle(QApplication::translate("MainWindow", "Save Map...", 0, QApplication::UnicodeUTF8));
        menuMap->setTitle(QApplication::translate("MainWindow", "Map", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
