========================================================================
       CONSOLE APPLICATION : HtmlLogger
========================================================================

This HtmlLogger app is a simple test for the CHTMLLogger class.  

I needed to create a portable ( Win32 - VXWorks ), thread-safe HTML log.
So, here it is, enjoy.

The work on the CHTMLLogger class is not complete. As a future enhancement the class 
shall support appending the existing log files. In addition button controls in the table 
cells can be a good idea to provide some feedback or other reactions.

There are a few middleware classes that helps hide a couple of nasty details, namely:
1) CGenFile - hides the file access details
2) CLogFont - generally an implementation of HTML font tag
3) CLogColor - an implementation of HTML color
4) CSmartString - a string class.

If you fill like some part (or all) of this code sucks I'll be glad to hear your oppinions.

Stas Desyatnikov
mailto: StasDesy@yahoo.com
