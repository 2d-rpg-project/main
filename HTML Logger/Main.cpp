// HtmlLogger.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "HtmlLogger.h"

int main(int argc, char* argv[])
{
	CHtmlLogger Log;
	if ( Log.Create( "FirstHMLLog.htm" ) )
	{
		if ( !Log.CreateTable( 3, "First HTML log" ) )
		{
			printf( "Failed to create log\n" );
			return -1;
		}

		Log.SetColumn( 0, "Heading1" );
		Log.SetColumn( 1, "Heading2" );
		Log.SetColumn( 2, "Heading3" );
		
		//Create white text
		//CLogFont font( 0x00FFFFFF, CLogFont::FW_DemiBold );
		//Black background
		//Log.SetCurrentLineColor( 0 );
		Log.AddLine( 0, CHtmlLogger::LT_Header );

		Log.SetColumn( 0, "1" );
		Log.SetColumn( 1, "2" );
		Log.SetColumn( 2, "3" );
		Log.AddLine();

		Log.SetColumn( 0, "3" );
		Log.SetColumn( 1, "4" );
		Log.SetColumn( 2, "5" );
		Log.AddLine();
		
		Log.AddLine();
		Log.SetColumn( 0, "First column" );
		Log.SetColumn( 1, "Second column" );
		Log.SetColumn( 2, "Third column" );
		Log.AddLine();

		//Set to red
		//Log.SetCurrentLineColor( 0x00FF0000 );
		Log.SetColumn( 0, "Error" );
		Log.SetColumn( 1, "Critical" );
		Log.SetColumn( 2, "Comment" );
		Log.AddLine();

		Log.SetColumn( 0, "Warning" );
		Log.SetColumn( 1, "Info" );
		Log.SetColumn( 2, "Comment" );
		Log.AddLine();
		
		Log.SetSummaryLine( "Well, average" );
		Log.AddLine( 0, CHtmlLogger::LT_Summary );

		//Add another table
		if ( !Log.CreateTable( 4, "Second HTML log" ) )
		{
			printf( "Failed to create log\n" );
			return -1;
		}
		
		Log.SetColumn( 0, "Step num" );
		Log.SetColumnWidth( 0, 2 ); // 2%

		Log.SetColumn( 1, "Command" );
		Log.SetColumn( 2, "Address" );
		Log.SetColumn( 3, "Mask/data" );
		CLogFont font( 0, CLogFont::FW_Medium );
		Log.AddLine( &font, CHtmlLogger::LT_Header );
		Log.SetCurrentLineColor( 0xFF0000 ); //Red
		Log.SetSummaryLine( "Test Failed" );
		Log.AddLine( 0, CHtmlLogger::LT_Summary );
	}
	
	return 0;
}

